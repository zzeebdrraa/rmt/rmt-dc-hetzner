#!/usr/bin/env bash
# set -x

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-dc-hetzner-hcloud.
#
# rmt-dc-hetzner is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-dc-hetzner is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-dc-hetzner. If not, see <https://www.gnu.org/licenses/>.

############################
# DEPENDENCIES
############################

# ./rmt-dc-hetzner-hcloud.dotfile

############################
# FUNCTIONS
############################

handle-list-server() {
  local isVerbose=0
  local hostNameListRegexList=()

  while [ "$1" ]; do
    case "$1" in
      -v|--verbose)
        isVerbose=1
        ;;
      *)
        # skip empty regexes or regexes only containing spaces
        [[ ! -z "${1//[[:space:]]/}" ]] && hostNameListRegexList+=("$1")
        ;;
    esac
    shift
  done

  [[ ${isVerbose} -eq 1 ]] && {
    local -r activeContext=$(hcloud-get-active-user-context)
    printf 'active project [%s]\n' "${activeContext:-could not be detected}"
  }
  ! hcloud-list-server "${isVerbose}" "${hostNameListRegexList[@]}" && return 1
  return 0
}