#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-dc-hetzner.
#
# rmt-dc-hetzner is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-dc-hetzner is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-dc-hetzner. If not, see <https://www.gnu.org/licenses/>.

############################
# INCLUDES
############################

# currently, dotfiles to test reside one level above bats files
declare -g -r __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../rmt-dc-hetzner-print.dotfile"
load "${__source_dir}/../../rmt-dc-hetzner-opts.dotfile"
load "${__source_dir}/../../rmt-dc-hetzner-cmd-info.dotfile"

############################
# TEST DATA
############################

declare -g -a -r __test_rmt_dc_hetzner_server_types_list=(
  'cx11  cx11'
  'cx11c cx11-ceph'
  'cx21  cx21'
  'cx21c cx21-ceph'
  'cx31  cx31'
  'cx31c cx31-ceph'
  'cx41  cx41'
  'cx41c cx41-ceph'
  'cx51  cx51'
  'cx51c cx51-ceph'
  'ccx11 ccx11'
  'ccx21 ccx21'
  'ccx31 ccx31'
  'ccx41 ccx41'
  'ccx51 ccx51'
)

declare -g -a -r __test_rmt_dc_hetzner_server_locations_list=(
  'n|nürnberg|nuremberg nbg1-dc3'
  'f|falkenstein        fsn1-dc14'
  'h|helsinki           hel1-dc2'
)

declare -g -a -r __test_rmt_dc_hetzner_server_images_list=(
  'u18|ubuntu-18.04   ubuntu-18.04'
  'u20|ubuntu-20.04   ubuntu-20.04'
  'u22|ubuntu-22.04   ubuntu-22.04'
  'd10|debian-10      debian-10'
  'd11|debian-11      debian-11'
  'c7|centos-7        centos-7'
  'c8|centos-8        centos-8'
  'f35|fedora-35      fedora-35'
)

############################
# TESTS
############################

@test "invoke handle-info" {
  run handle-info l
  [ ${status} -eq 0 ]
  
  local index line
  for index in "${!lines[@]}"; do
    [ "${lines[${index}]}" == "${__test_rmt_dc_hetzner_server_locations_list[${index}]}" ]
  done
  
  run handle-info location
  [ ${status} -eq 0 ]
  for index in "${!lines[@]}"; do
    [ "${lines[${index}]}" == "${__test_rmt_dc_hetzner_server_locations_list[${index}]}" ]
  done
  
  run handle-info t
  [ ${status} -eq 0 ]
  for index in "${!lines[@]}"; do
    [ "${lines[${index}]}" == "${__test_rmt_dc_hetzner_server_types_list[${index}]}" ]
  done
  
  run handle-info type
  [ ${status} -eq 0 ]
  for index in "${!lines[@]}"; do
    [ "${lines[${index}]}" == "${__test_rmt_dc_hetzner_server_types_list[${index}]}" ]
  done
  
  run handle-info i
  [ ${status} -eq 0 ]
  for index in "${!lines[@]}"; do
    [ "${lines[${index}]}" == "${__test_rmt_dc_hetzner_server_images_list[${index}]}" ]
  done
  
  run handle-info image
  [ ${status} -eq 0 ]
  for index in "${!lines[@]}"; do
    [ "${lines[${index}]}" == "${__test_rmt_dc_hetzner_server_images_list[${index}]}" ]
  done
}

@test "invoke handle-info - no args" {
  run handle-info
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
}

@test "invoke handle-info - unknown args" {
  run handle-info unknown
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}