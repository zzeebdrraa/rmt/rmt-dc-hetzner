#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-dc-hetzner.
#
# rmt-dc-hetzner is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-dc-hetzner is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-dc-hetzner. If not, see <https://www.gnu.org/licenses/>.

############################
# INCLUDES
############################

# currently, dotfiles to test reside one level above bats files
declare -g -r __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../../common/map.dotfile"
load "${__source_dir}/../../rmt-dc-hetzner-opts.dotfile"

############################
# TEST DATA
############################

declare -r -g -a __test_rmt_dc_hetzner_server_types_list=(
  'cx11  cx11'
  'cx11c cx11-ceph'
  'cx21  cx21'
  'cx21c cx21-ceph'
  'cx31  cx31'
  'cx31c cx31-ceph'
  'cx41  cx41'
  'cx41c cx41-ceph'
  'cx51  cx51'
  'cx51c cx51-ceph'
  'ccx11 ccx11'
  'ccx21 ccx21'
  'ccx31 ccx31'
  'ccx41 ccx41'
  'ccx51 ccx51'
)

declare -r -g -a __test_rmt_dc_hetzner_server_locations_list=(
  'n|nürnberg|nuremberg nbg1-dc3'
  'f|falkenstein        fsn1-dc14'
  'h|helsinki           hel1-dc2'
)

declare -r -g -a __test_rmt_dc_hetzner_server_images_list=(
  'u18|ubuntu-18.04   ubuntu-18.04'
  'u20|ubuntu-20.04   ubuntu-20.04'
  'u22|ubuntu-22.04   ubuntu-22.04'
  'd10|debian-10      debian-10'
  'd11|debian-11      debian-11'
  'c7|centos-7        centos-7'
  'c8|centos-8        centos-8'
  'f35|fedora-35      fedora-35'
)

############################
# TESTS
############################

@test "invoke get-hetzner-server-type" {
  transform-multi-key-value-list-to-map __test_rmt_dc_hetzner_server_types_list __rmt_dc_hetzner_server_types_map

  local entry key value
  for entry in "${__test_rmt_dc_hetzner_server_types_list[@]}"; do
    read -r key value <<<"${entry}"
    run get-hetzner-server-type "${key}"
    [ ${status} -eq 0 ]
    [ "${output}" == "${value}" ]
  done
}

@test "invoke get-hetzner-server-type - error - unknown type" {
  transform-multi-key-value-list-to-map __test_rmt_dc_hetzner_server_types_list __rmt_dc_hetzner_server_types_map

  run get-hetzner-server-type 'unknown'
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no server-type available for key [unknown]' ]
}

@test "invoke get-hetzner-server-type - error - no map" {
  run get-hetzner-server-type 'cx11'
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no server-type available for key [cx11]' ]
}

@test "invoke get-hetzner-server-type - error - no args" {
  transform-multi-key-value-list-to-map __test_rmt_dc_hetzner_server_types_list __rmt_dc_hetzner_server_types_map

  run get-hetzner-server-type
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run get-hetzner-server-type ''
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke get-hetzner-server-location" {
  transform-multi-key-value-list-to-map __test_rmt_dc_hetzner_server_locations_list __rmt_dc_hetzner_server_locations_map

  local entry keysStr key value keysList=() 
  for entry in "${__test_rmt_dc_hetzner_server_locations_list[@]}"; do
    read -r keysStr value <<<"${entry}"
    IFS='|' read -r -a keysList <<<"${keysStr}"

    for key in "${keysList[@]}"; do
      run get-hetzner-server-location "${key}"
      [ ${status} -eq 0 ]
      [ "${output}" == "${value}" ]
    done
  done
}

@test "invoke get-hetzner-server-location - random" {
  transform-multi-key-value-list-to-map __test_rmt_dc_hetzner_server_locations_list __rmt_dc_hetzner_server_locations_map

  run get-hetzner-server-location 'r'
  [ ${status} -eq 0 ]
  # [ "${output}" == "${value}" ]
  
  run get-hetzner-server-location 'random'
  [ ${status} -eq 0 ]
  # [ "${output}" == "${value}" ]
}

@test "invoke get-hetzner-server-location - error - unknown location" {
  transform-multi-key-value-list-to-map __test_rmt_dc_hetzner_server_locations_list __rmt_dc_hetzner_server_locations_map

  run get-hetzner-server-location 'unknown'
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no server-location available for key [unknown]' ]
}

@test "invoke get-hetzner-server-location - error - no map" {
  run get-hetzner-server-location 'helsinki'
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no server-location available for key [helsinki]' ]
}

@test "invoke get-hetzner-server-location - error - no args" {
  transform-multi-key-value-list-to-map __test_rmt_dc_hetzner_server_locations_list __rmt_dc_hetzner_server_locations_map

  run get-hetzner-server-location
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run get-hetzner-server-location ''
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke get-hetzner-server-os-image" {
  transform-multi-key-value-list-to-map __test_rmt_dc_hetzner_server_images_list __rmt_dc_hetzner_server_images_map
  
  local entry keysStr key value keysList=() 
  for entry in "${__test_rmt_dc_hetzner_server_images_list[@]}"; do
    read -r keysStr value <<<"${entry}"
    IFS='|' read -r -a keysList <<<"${keysStr}"

    for key in "${keysList[@]}"; do
      run get-hetzner-server-os-image "${key}"
      [ ${status} -eq 0 ]
      [ "${output}" == "${value}" ]
    done
  done
}

@test "invoke get-hetzner-server-os-image - error - unknown image" {
  transform-multi-key-value-list-to-map __test_rmt_dc_hetzner_server_images_list __rmt_dc_hetzner_server_images_map

  run get-hetzner-server-os-image 'unknown'
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no server-image available for key [unknown]' ]
}

@test "invoke get-hetzner-server-os-image - error - no map" {
  run get-hetzner-server-os-image 'debian-11'
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no server-image available for key [debian-11]' ]
}

@test "invoke get-hetzner-server-os-image - error - no args" {
  transform-multi-key-value-list-to-map __test_rmt_dc_hetzner_server_images_list __rmt_dc_hetzner_server_images_map

  run get-hetzner-server-os-image
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run get-hetzner-server-os-image ''
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}