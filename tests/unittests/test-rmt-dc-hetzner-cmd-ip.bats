#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-dc-hetzner.
#
# rmt-dc-hetzner is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-dc-hetzner is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-dc-hetzner. If not, see <https://www.gnu.org/licenses/>.

############################
# INCLUDES
############################

# currently, dotfiles to test reside one level above bats files
declare -r -g __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../rmt-dc-hetzner-hcloud.dotfile"
load "${__source_dir}/../../rmt-dc-hetzner-cmd-ip.dotfile"

load "${__source_dir}/../../../common-test/test-rmt-mocks-common"
load "${__source_dir}/../../../common-test/test-rmt-utilities"

load "${__source_dir}/test-rmt-mocks-hcloud"

############################
# TEST DATA
############################

declare -r -g __test_data_dir='test-data'
# overwrite definition in test-rmt-mocks-common
__test_file_buffer="${__test_data_dir}/test-hcloud-file_buffer"

############################
# SETUP
############################

setup() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
  [[ -f "${__test_file_buffer}" ]] && rm "${__test_file_buffer}"

  mkdir "${__test_data_dir}"
}

teardown() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
}

############################
# TESTS
############################

@test "invoke handle-list-ip - options only" {
  __hcloud_cmd=mock-cmd-ok-append-args-to-file

  local listOut=()
  run handle-list-ip ipv4
  [ ${status} -eq 0 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  
  [ "${optsList[0]}" == 'server' ]
  [ "${optsList[1]}" == 'list' ]
  [ "${optsList[2]}" == '-o' ]
  [ "${optsList[3]}" == 'columns=name,ipv4' ]
  [ "${optsList[4]}" == '-o' ]
  [ "${optsList[5]}" == 'noheader' ]
  
  rm "${__test_file_buffer}"
  
  run handle-list-ip ipv6
  [ ${status} -eq 0 ]
  
  optsList=()
  read-file-buffer "${__test_file_buffer}" optsList

  [ "${optsList[0]}" == 'server' ]
  [ "${optsList[1]}" == 'list' ]
  [ "${optsList[2]}" == '-o' ]
  [ "${optsList[3]}" == 'columns=name,ipv6' ]
  [ "${optsList[4]}" == '-o' ]
  [ "${optsList[5]}" == 'noheader' ]
  
  rm "${__test_file_buffer}"
  
  run handle-list-ip ipv6 -v
  [ ${status} -eq 0 ]
  
  optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  
  [ "${optsList[0]}" == 'context' ]
  [ "${optsList[1]}" == 'list' ]
  [ "${optsList[2]}" == '-o' ]
  [ "${optsList[3]}" == 'noheader' ]
  [ "${optsList[4]}" == 'server' ]
  [ "${optsList[5]}" == 'list' ]
  [ "${optsList[6]}" == '-o' ]
  [ "${optsList[7]}" == 'columns=name,ipv6' ]
  [ "${optsList[8]}" == '-o' ]
  [ "${optsList[9]}" == 'noheader' ]
}

@test "invoke handle-list-ip - ipv4" {
  __hcloud_cmd=mock-hcloud-list-server-ip-one

  run handle-list-ip ipv4 'testhost'
  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ "${output}" == '127.0.0.1' ]
  
  __hcloud_cmd=mock-hcloud-list-server-ip-three
  
  run handle-list-ip ipv4 'host'
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '127.0.0.1' ]
  [ "${lines[1]}" == '127.0.0.2' ]
  [ "${lines[2]}" == '127.0.0.3' ]

  run handle-list-ip ipv4 'example'
  [ ${status} -eq 0 ]
  [ "${output}" == '127.0.0.3' ]
  
  run handle-list-ip ipv4 'local' 'test'
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '127.0.0.1' ]
  [ "${lines[1]}" == '127.0.0.2' ]
}

@test "invoke handle-list-ip - ipv6" {
  __hcloud_cmd=mock-hcloud-list-server-ip-one

  run handle-list-ip ipv6 'testhost'
  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ "${output}" == '2001:4dd0:100:1020:53:2::2' ]
  
  __hcloud_cmd=mock-hcloud-list-server-ip-three
  
  run handle-list-ip ipv6 'host'
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '2001:4dd0:100:1020:53:2::1' ]
  [ "${lines[1]}" == '2001:4dd0:100:1020:53:2::2' ]
  [ "${lines[2]}" == '2001:4dd0:100:1020:53:2::3' ]

  run handle-list-ip ipv6 'example'
  [ ${status} -eq 0 ]
  [ "${output}" == '2001:4dd0:100:1020:53:2::3' ]
  
  run handle-list-ip ipv6 'local' 'test'
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '2001:4dd0:100:1020:53:2::1' ]
  [ "${lines[1]}" == '2001:4dd0:100:1020:53:2::2' ]
}

@test "invoke handle-list-ip - ipv4 - no args" {
  __hcloud_cmd=mock-hcloud-list-server-ip-three-and-active-user-context

  run handle-list-ip ipv4
  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '127.0.0.1' ]
  [ "${lines[1]}" == '127.0.0.2' ]
  [ "${lines[2]}" == '127.0.0.3' ]
  
  run handle-list-ip ipv4 -v
  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'active project [testuser]' ]
  [ "${lines[1]}" == '127.0.0.1' ]
  [ "${lines[2]}" == '127.0.0.2' ]
  [ "${lines[3]}" == '127.0.0.3' ]
}

@test "invoke handle-list-ip - ipv6 - no args" {
  __hcloud_cmd=mock-hcloud-list-server-ip-three-and-active-user-context

  run handle-list-ip ipv6
  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '2001:4dd0:100:1020:53:2::1' ]
  [ "${lines[1]}" == '2001:4dd0:100:1020:53:2::2' ]
  [ "${lines[2]}" == '2001:4dd0:100:1020:53:2::3' ]
  
  run handle-list-ip ipv6 -v
  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'active project [testuser]' ]
  [ "${lines[1]}" == '2001:4dd0:100:1020:53:2::1' ]
  [ "${lines[2]}" == '2001:4dd0:100:1020:53:2::2' ]
  [ "${lines[3]}" == '2001:4dd0:100:1020:53:2::3' ]
}

@test "invoke handle-list-ip - ipv4 - no match" {
  __hcloud_cmd=mock-hcloud-list-server-ip-three-and-active-user-context

  run handle-list-ip ipv4 'unknown'
  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run handle-list-ip ipv4 -v 'unknown'
  # printf '# l2 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'active project [testuser]' ]
  [ "${lines[1]}" == 'debug: no running servers' ]
  
  __hcloud_cmd=mock-cmd-ok-no-output

  run handle-list-ip ipv4 'testhost'
  # printf '# l3 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run handle-list-ip ipv4 -v 'testhost'
  # printf '# l4 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'active project [could not be detected]' ]
  [ "${lines[1]}" == 'debug: no running servers' ]
}

@test "invoke handle-list-ip - ipv6 - no match" {
  __hcloud_cmd=mock-hcloud-list-server-ip-three-and-active-user-context

  run handle-list-ip ipv6 'unknown'
  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run handle-list-ip ipv6 -v 'unknown'
  # printf '# l2 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'active project [testuser]' ]
  [ "${lines[1]}" == 'debug: no running servers' ]
  
  __hcloud_cmd=mock-cmd-ok-no-output

  run handle-list-ip ipv6 'testhost'
  # printf '# l3 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run handle-list-ip ipv6 -v 'testhost'
  # printf '# l4 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'active project [could not be detected]' ]
  [ "${lines[1]}" == 'debug: no running servers' ]
}

@test "invoke handle-list-ip - ipv4 - error" {
  skip 'error not yet detectable'
  __hcloud_cmd=mock-cmd-error

  run handle-list-ip ipv4
  [ ${status} -eq 1 ]
}

@test "invoke handle-list-ip - ipv6 - error" {
  skip 'error not yet detectable'
  __hcloud_cmd=mock-cmd-error

  run handle-list-ip ipv6
  [ ${status} -eq 1 ]
}