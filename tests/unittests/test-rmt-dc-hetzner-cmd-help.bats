#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-dc-hetzner.
#
# rmt-dc-hetzner is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-dc-hetzner is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-dc-hetzner. If not, see <https://www.gnu.org/licenses/>.

############################
# INCLUDES
############################

# currently, dotfiles to test reside one level above bats files
declare -g -r __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../rmt-dc-hetzner-data-help.dotfile"
load "${__source_dir}/../../rmt-dc-hetzner-cmd-help.dotfile"

############################
# TESTS
############################

@test "invoke handle-help" {
  run handle-help s
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
  
  run handle-help short
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
  
  run handle-help d
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
  
  run handle-help details
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
}

@test "invoke handle-help - no args" {
  run handle-help
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
}

@test "invoke handle-help - unknown args" {
  run handle-help unknown
  [ ${status} -eq 0 ]
  [ ! -z "${output}" ]
}