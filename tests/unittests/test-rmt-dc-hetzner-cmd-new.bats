#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-dc-hetzner.
#
# rmt-dc-hetzner is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-dc-hetzner is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-dc-hetzner. If not, see <https://www.gnu.org/licenses/>.

############################
# INCLUDES
############################

# currently, dotfiles to test reside one level above bats files
declare -r -g __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../../common/fifo.dotfile"
load "${__source_dir}/../../../common/lists.dotfile"
load "${__source_dir}/../../../common/math.dotfile"
load "${__source_dir}/../../../common/map.dotfile"
load "${__source_dir}/../../../common/net.dotfile"
load "${__source_dir}/../../../common/parallel.dotfile"
load "${__source_dir}/../../../common/strings.dotfile"
load "${__source_dir}/../../../common/tmp.dotfile"
load "${__source_dir}/../../../common/types.dotfile"

load "${__source_dir}/../../rmt-dc-hetzner-data-common.dotfile"
load "${__source_dir}/../../rmt-dc-hetzner-hcloud.dotfile"
load "${__source_dir}/../../rmt-dc-hetzner-sshkeygen.dotfile"
load "${__source_dir}/../../rmt-dc-hetzner-opts.dotfile"
load "${__source_dir}/../../rmt-dc-hetzner-print.dotfile"
load "${__source_dir}/../../rmt-dc-hetzner-cmd-new.dotfile"

load "${__source_dir}/../../../common-test/test-rmt-asserts-common"
load "${__source_dir}/../../../common-test/test-rmt-mocks-common"
load "${__source_dir}/../../../common-test/test-rmt-utilities"

load "${__source_dir}/test-rmt-mocks-hcloud"

############################
# TEST DATA
############################

declare -r -g __test_data_dir=$(mktemp -d -u)
declare -r -g __test_pipe_buffer="${__test_data_dir}/test-hcloud-pipe-buffer"

declare -r -g __test_expected_callstack_args_new_disable_rescue=(
  # hcloud
  'ssh-key'
  'list'
  '-o'
  'noheader'
  '-o'
  'columns=name'
  # hcloud
  'server'
  'list'
  '-o'
  'noheader'
  '-o'
  'columns=name,ipv4,status,rescue_enabled,datacenter,type'
  # hcloud
  'server'
  'create'
  '--name'
  'testhost'
  '--type'
  'cx11'
  '--datacenter'
  'nbg1-dc3'
  '--image'
  'debian-11'
  '--ssh-key'
  'key-user-a'
  '--ssh-key'
  'key-user-b'
  '--ssh-key'
  'key-user-c'
  # hcloud
  'server'
  'list'
  '-o'
  'noheader'
  '-o'
  'columns=name,ipv4,status,rescue_enabled,datacenter,type'
  # sshkeygen
  '-f'
  "${__rmt_dc_hetzner_known_hosts_file_default}"
  '-R'
  '128.0.0.1'
  # netcat
  '-w'
  '1'
  '-zn'
  '128.0.0.1'
  '22'
)

declare -r -g __test_expected_callstack_args_new_enable_rescue=(
  # hcloud
  'ssh-key'
  'list'
  '-o'
  'noheader'
  '-o'
  'columns=name'
  # hcloud
  'server'
  'list'
  '-o'
  'noheader'
  '-o'
  'columns=name,ipv4,status,rescue_enabled,datacenter,type'
  # hcloud
  'server'
  'create'
  '--name'
  'testhost'
  '--type'
  'cx11'
  '--datacenter'
  'nbg1-dc3'
  '--image'
  'debian-11'
  '--ssh-key'
  'key-user-a'
  '--ssh-key'
  'key-user-b'
  '--ssh-key'
  'key-user-c'
  # hcloud
  'server'
  'list'
  '-o'
  'noheader'
  '-o'
  'columns=name,ipv4,status,rescue_enabled,datacenter,type'
  # sshkeygen
  '-f'
  "${__rmt_dc_hetzner_known_hosts_file_default}"
  '-R'
  '128.0.0.1'
  # netcat
  '-w'
  '1'
  '-zn'
  '128.0.0.1'
  '22'
  # hcloud
  'server'
  'enable-rescue'
  'testhost'
  # hcloud
  'server'
  'poweroff'
  'testhost'
  # netcat
  '-w'
  '1'
  '-zn'
  '128.0.0.1'
  '22'
  # hcloud
  'server'
  'poweron'
  'testhost'
  # netcat
  '-w'
  '1'
  '-zn'
  '128.0.0.1'
  '22'
  # sshkeygen
  '-f'
  "${__rmt_dc_hetzner_known_hosts_file_default}"
  '-R'
  '128.0.0.1'
)

declare -r -g __test_expected_callstack_args_existing_normal_mode_disable_rescue=(
  # hcloud
  'ssh-key'
  'list'
  '-o'
  'noheader'
  '-o'
  'columns=name'
  # hcloud
  'server'
  'list'
  '-o'
  'noheader'
  '-o'
  'columns=name,ipv4,status,rescue_enabled,datacenter,type'
  # netcat
  '-w'
  '1'
  '-zn'
  '128.0.0.1'
  '22'
)

declare -r -g __test_expected_callstack_args_existing_normal_mode_enable_rescue=(
  # hcloud
  'ssh-key'
  'list'
  '-o'
  'noheader'
  '-o'
  'columns=name'
  # hcloud
  'server'
  'list'
  '-o'
  'noheader'
  '-o'
  'columns=name,ipv4,status,rescue_enabled,datacenter,type'
  #netcat
  '-w'
  '1'
  '-zn'
  '128.0.0.1'
  '22'
  # hcloud
  'server'
  'enable-rescue'
  'testhost'
  # hcloud
  'server'
  'poweroff'
  'testhost'
  # netcat
  '-w'
  '1'
  '-zn'
  '128.0.0.1'
  '22'
  # hcloud
  'server'
  'poweron'
  'testhost'
  # netcat
  '-w'
  '1'
  '-zn'
  '128.0.0.1'
  '22'
  # sshkeygen
  '-f'
  "${__rmt_dc_hetzner_known_hosts_file_default}"
  '-R'
  '128.0.0.1'
)

declare -r -g __test_expected_callstack_args_existing_in_rescue_mode_error=(
  # hcloud
  'ssh-key'
  'list'
  '-o'
  'noheader'
  '-o'
  'columns=name'
  # hcloud
  'server'
  'list'
  '-o'
  'noheader'
  '-o'
  'columns=name,ipv4,status,rescue_enabled,datacenter,type'
)

############################
# MOCKS
############################

mock-common-mktemp() {
  printf '%s\n' "${__test_data_dir}"
}

############################
# Setup
############################

setup() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
  mkdir -p "${__test_data_dir}"

  # overwrite definition in common-test/test-rmt-mocks-common
  __test_file_buffer="${__test_data_dir}/test-hcloud-file-buffer"
  
  # overwrite definition in test-rmt-mocks-hcloud
  __test_new_server_info_counter_buffer="${__test_data_dir}/test-new-server-info-counter-buffer"
  
  # overwrite definition in common/tmp.dotfile
  # no need to create a real temp dir in this command as a temp-dir
  # is created already above by mkdir "${__test_data_dir}"
  __mktemp_cmd=mock-common-mktemp

  transform-multi-key-value-list-to-map __rmt_dc_hetzner_server_images_list __rmt_dc_hetzner_server_images_map
  transform-multi-key-value-list-to-map __rmt_dc_hetzner_server_locations_list __rmt_dc_hetzner_server_locations_map
  transform-multi-key-value-list-to-map __rmt_dc_hetzner_server_types_list __rmt_dc_hetzner_server_types_map
}

teardown() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
}

############################
# Tests
############################

#***************************
# handle-new-server
#***************************

@test "invoke handle-new-server - options only - new - disable rescue mode" {

  __hcloud_cmd=mock-hcloud-new-server-new-disable-rescue-mode-and-opts
  __sshkeygen_cmd=mock-cmd-ok-append-args-to-file
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one-and-opts

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=0

  run handle-new-server -t 'cx11' -l 'n' -i 'd11' 'testhost'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  [ "${#optsList[*]}" == "${#__test_expected_callstack_args_new_disable_rescue[*]}" ]

  run assert-lists-equal __test_expected_callstack_args_new_disable_rescue optsList
  [ ${status} -eq 0 ]
}

@test "invoke handle-new-server - options only - new - enable rescue mode" {

  __hcloud_cmd=mock-hcloud-new-server-new-enable-rescue-mode-and-opts
  __sshkeygen_cmd=mock-cmd-ok-append-args-to-file
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one-and-opts

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=0

  run handle-new-server --re -t 'cx11' -l 'n' -i 'd11' 'testhost'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  # printf '# o [%s]\n' "${optsList[@]}" >&3
  [ "${#optsList[*]}" == "${#__test_expected_callstack_args_new_enable_rescue[*]}" ]

  run assert-lists-equal __test_expected_callstack_args_new_enable_rescue optsList
  [ ${status} -eq 0 ]
}

@test "invoke handle-new-server - options only - existing - normal mode - disable rescue mode" {

  __hcloud_cmd=mock-hcloud-new-server-existing-normal-disable-rescue-mode-and-opts
  # mock-cmd-ok-no-output is ok as __sshkeygen_cmd is not supposed to be invoked
  # if a server already exists
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one-and-opts

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=0

  run handle-new-server -t 'cx11' -l 'n' -i 'd11' 'testhost'
  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  [ "${#optsList[*]}" == "${#__test_expected_callstack_args_existing_normal_mode_disable_rescue[*]}" ]

  run assert-lists-equal __test_expected_callstack_args_existing_normal_mode_disable_rescue optsList
  [ ${status} -eq 0 ]
}

@test "invoke handle-new-server - options only - existing - normal mode - enable rescue mode" {

  __hcloud_cmd=mock-hcloud-new-server-existing-normal-enable-rescue-mode-and-opts
  __sshkeygen_cmd=mock-cmd-ok-append-args-to-file
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one-and-opts

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=0

  run handle-new-server --re -t 'cx11' -l 'n' -i 'd11' 'testhost'
  [ ${status} -eq 0 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  [ "${#optsList[*]}" == "${#__test_expected_callstack_args_existing_normal_mode_enable_rescue[*]}" ]

  run assert-lists-equal __test_expected_callstack_args_existing_normal_mode_enable_rescue optsList
  [ ${status} -eq 0 ]
}

@test "invoke handle-new-server - options only - existing - rescue mode - disable rescue mode - error" {

  __hcloud_cmd=mock-hcloud-new-server-existing-in-recue-mode-and-opts
  # mock-cmd-ok-no-output is ok as __sshkeygen_cmd is not supposed to be invoked
  # if a server already exists
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one-and-opts

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=1

  run handle-new-server -v -t 'cx11' -l 'n' -i 'd11' 'testhost'
  [ ${status} -eq 1 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  [ "${#optsList[*]}" == "${#__test_expected_callstack_args_existing_in_rescue_mode_error[*]}" ]
  
  run assert-lists-equal __test_expected_callstack_args_existing_in_rescue_mode_error optsList
  [ ${status} -eq 0 ]
}

@test "invoke handle-new-server - options only - existing - rescue mode - enable rescue mode - error" {

  __hcloud_cmd=mock-hcloud-new-server-existing-in-recue-mode-and-opts
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one-and-opts

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=1

  run handle-new-server --re -t 'cx11' -l 'n' -i 'd11' 'testhost'
  [ ${status} -eq 1 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  [ "${#optsList[*]}" == "${#__test_expected_callstack_args_existing_in_rescue_mode_error[*]}" ]
  
  run assert-lists-equal __test_expected_callstack_args_existing_in_rescue_mode_error optsList
  [ ${status} -eq 0 ]
}

@test "invoke handle-new-server - new - disable rescue mode" {

  __hcloud_cmd=mock-hcloud-new-server-new-disable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=0

  run handle-new-server -t 'cx11' -l 'n' -i 'd11' 'testhost'
  # printf '# %s\n' "${lines[@]}" >&3
  
  [ ${status} -eq 0 ]
  [ "${output}" == 'testhost 128.0.0.1' ]
}

@test "invoke handle-new-server - new - disable rescue mode - verbose" {

  __hcloud_cmd=mock-hcloud-new-server-new-disable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=0
  
  run handle-new-server --verbose -t 'cx11' -l 'n' -i 'd11' 'testhost'
  [ ${status} -eq 0 ]

  [ "${lines[0]}" == 'debug: create host [testhost] ...' ]
  [[ "${lines[1]}" =~ (debug: wait until host creation has been completed for \[testhost\] \[[0-9]*\] ...) ]]
  [ "${lines[2]}" == 'debug: wait for server [testhost] to be reachable ...' ]
  [ "${lines[3]}" == 'hetzner server:' ]
  [ "${lines[4]}" == '--------------' ]
  [ "${lines[5]}" == 'name        : testhost' ]
  [ "${lines[6]}" == 'ip          : 128.0.0.1' ]
  [ "${lines[7]}" == 'type        : cx11' ]
  [ "${lines[8]}" == 'location    : nbrg' ]
  [ "${lines[9]}" == 'rescue mode : 0' ]
  [ "${lines[10]}" == 'ssh-auth    : key-user-a key-user-b key-user-c' ]
  [ "${lines[11]}" == '--------------' ]
}

@test "invoke handle-new-server - new - enable rescue mode" {

  __hcloud_cmd=mock-hcloud-new-server-new-enable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=0

  run handle-new-server --re -t 'cx11' -l 'n' -i 'd11' 'testhost'
  [ ${status} -eq 0 ]
  [ "${output}" == 'testhost 128.0.0.1 testpwd' ]
}

@test "invoke handle-new-server - new - enable rescue mode - verbose" {

  __hcloud_cmd=mock-hcloud-new-server-new-enable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=0
  
  run handle-new-server --verbose --re -t 'cx11' -l 'n' -i 'd11' 'testhost'
  # printf '# l2 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  [ "${lines[0]}" == 'debug: create host [testhost] ...' ]
  [[ "${lines[1]}" =~ (debug: wait until host creation has been completed for \[testhost\] \[[0-9]*\] ...) ]]
  [ "${lines[2]}" == 'debug: wait for server [testhost] to be reachable ...' ]
  [ "${lines[3]}" == 'debug: reboot into rescue mode and wait for server [testhost] to be reachable ...' ]
  [ "${lines[4]}" == 'hetzner server (rescue mode):' ]
  [ "${lines[5]}" == '--------------' ]
  [ "${lines[6]}" == 'name        : testhost' ]
  [ "${lines[7]}" == 'ip          : 128.0.0.1' ]
  [ "${lines[8]}" == 'type        : cx11' ]
  [ "${lines[9]}" == 'location    : nbrg' ]
  [ "${lines[10]}" == 'rescue-pwd  : testpwd' ]
  [ "${lines[11]}" == '--------------' ]
}

@test "invoke handle-new-server - existing - normal mode - disable rescue mode" {

  __hcloud_cmd=mock-hcloud-new-server-existing-normal-disable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=0

  run handle-new-server -t 'cx11' -l 'n' -i 'd11' 'testhost'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  [ "${lines[0]}" == 'warning: requested server [testhost] does already exist. proceeding with existing server...' ]
  [ "${lines[1]}" == 'testhost 128.0.0.1' ]
}

@test "invoke handle-new-server - existing - normal mode - disable rescue mode - verbose" {

  __hcloud_cmd=mock-hcloud-new-server-existing-normal-disable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=0
  
  run handle-new-server --verbose -t 'cx11' -l 'n' -i 'd11' 'testhost'
  # printf '# l (%s)\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  [ "${lines[0]}"  == 'debug: create host [testhost] ...' ]
  [[ "${lines[1]}" =~ (debug: wait until host creation has been completed for \[testhost\] \[[0-9]*\] ...) ]]
  [ "${lines[2]}"  == "warning: requested server [testhost] does already exist. proceeding with existing server..." ]
  [ "${lines[3]}"  == 'debug: wait for server [testhost] to be reachable ...' ]
  [ "${lines[4]}"  == 'hetzner server:' ]
  [ "${lines[5]}"  == '--------------' ]
  [ "${lines[6]}"  == 'name        : testhost' ]
  [ "${lines[7]}"  == 'ip          : 128.0.0.1' ]
  [ "${lines[8]}"  == 'type        : cx11' ]
  [ "${lines[9]}"  == 'location    : nbrg' ]
  [ "${lines[10]}" == 'rescue mode : 0' ]
  [ "${lines[11]}" == 'ssh-auth    : key-user-a key-user-b key-user-c' ]
  [ "${lines[12]}" == '--------------' ]
}

@test "invoke handle-new-server - existing - normal mode - enable rescue mode" {

  __hcloud_cmd=mock-hcloud-new-server-existing-normal-enable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=0
  
  run handle-new-server --re -t 'cx11' -l 'n' -i 'd11' 'testhost'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  [ "${lines[0]}" == 'warning: requested server [testhost] does already exist. proceeding with existing server...' ]
  [ "${lines[1]}" == 'testhost 128.0.0.1 testpwd' ]
}

@test "invoke handle-new-server - existing - normal mode - enable rescue mode - verbose" {

  __hcloud_cmd=mock-hcloud-new-server-existing-normal-enable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=0
  
  run handle-new-server --verbose --re -t 'cx11' -l 'n' -i 'd11' 'testhost'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]

  [ "${lines[0]}"  == 'debug: create host [testhost] ...' ]
  [[ "${lines[1]}" =~ (debug: wait until host creation has been completed for \[testhost\] \[[0-9]*\] ...) ]]
  [ "${lines[2]}"  == "warning: requested server [testhost] does already exist. proceeding with existing server..." ]
  [ "${lines[3]}"  == 'debug: wait for server [testhost] to be reachable ...' ]
  [ "${lines[4]}"  == 'debug: reboot into rescue mode and wait for server [testhost] to be reachable ...' ]
  [ "${lines[5]}"  == 'hetzner server (rescue mode):' ]
  [ "${lines[6]}"  == '--------------' ]
  [ "${lines[7]}"  == 'name        : testhost' ]
  [ "${lines[8]}"  == 'ip          : 128.0.0.1' ]
  [ "${lines[9]}"  == 'type        : cx11' ]
  [ "${lines[10]}"  == 'location    : nbrg' ]
  [ "${lines[11]}" == 'rescue-pwd  : testpwd' ]
  [ "${lines[12]}" == '--------------' ]
}

@test "invoke handle-new-server - existing - rescue mode - disable rescue mode - error" {

  __hcloud_cmd=mock-hcloud-new-server-existing-in-recue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=1

  run handle-new-server -t 'cx11' -l 'n' -i 'd11' 'testhost'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]

  [ "${lines[0]}"  == 'error: requested server [testhost] does already exist and is in rescue mode already.' ]
  [ "${lines[1]}"  == 'error: turn off rescue mode first and try again.' ]
  [[ "${lines[2]}" =~ (error: host creation failed for \[testhost\] \[[0-9]+\]) ]]
}

@test "invoke handle-new-server - existing - rescue mode - disable rescue mode - error - verbose" {

  __hcloud_cmd=mock-hcloud-new-server-existing-in-recue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=1

  run handle-new-server -t 'cx11' -l 'n' -i 'd11' 'testhost'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]

  [ "${lines[0]}"  == 'error: requested server [testhost] does already exist and is in rescue mode already.' ]
  [ "${lines[1]}"  == 'error: turn off rescue mode first and try again.' ]
  [[ "${lines[2]}" =~ (error: host creation failed for \[testhost\] \[[0-9]+\]) ]]
}

@test "invoke handle-new-server - existing - rescue mode - enable rescue mode - error" {

  __hcloud_cmd=mock-hcloud-new-server-existing-in-recue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=1

  run handle-new-server --re -t 'cx11' -l 'n' -i 'd11' 'testhost'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]

  [ "${lines[0]}"  == 'error: requested server [testhost] does already exist and is in rescue mode already.' ]
  [ "${lines[1]}"  == 'error: turn off rescue mode first and try again.' ]
  [[ "${lines[2]}" =~ (error: host creation failed for \[testhost\] \[[0-9]+\]) ]]
}

@test "invoke handle-new-server - existing - rescue mode - enable rescue mode - error - verbose" {

  __hcloud_cmd=mock-hcloud-new-server-existing-in-recue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=1

  run handle-new-server --verbose --re -t 'cx11' -l 'n' -i 'd11' 'testhost'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  
  [ "${lines[0]}"  == 'debug: create host [testhost] ...' ]
  [[ "${lines[1]}" =~ (debug: wait until host creation has been completed for \[testhost\] \[[0-9]+\] ...) ]]
  [ "${lines[2]}"  == 'error: requested server [testhost] does already exist and is in rescue mode already.' ]
  [ "${lines[3]}"  == 'error: turn off rescue mode first and try again.' ]
  [[ "${lines[4]}" =~ (error: host creation failed for \[testhost\] \[[0-9]+\]) ]]
}

@test "invoke handle-new-server - missing args - error" {
  run handle-new-server
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: no host name has been given' ]
}

@test "invoke handle-new-server - wrong args - error" {

  run handle-new-server -t 'cxx' 'testhost'
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: no server-type available for key [cxx]' ]
  [ "${lines[1]}" == 'error: hetzner host type [cxx] is not supported' ]

  run handle-new-server -l 'unknown' 'testhost'
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: no server-location available for key [unknown]' ]
  [ "${lines[1]}" == 'error: hetzner datacenter location [unknown] is not supported' ]

  run handle-new-server -i 'alpine' 'testhost'
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: no server-image available for key [alpine]' ]
  [ "${lines[1]}" == 'error: hetzner os image [alpine] is not supported' ]
  
  run handle-new-server '   '
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: none of the given host names is valid [   ]' ]
}

@test "invoke handle-new-server - error" {
  __hcloud_cmd=mock-cmd-error
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  run handle-new-server 'testhost'
  [ ${status} -eq 1 ]

  [[ "${lines[0]}" =~ (error: host creation failed for \[testhost\] \[[0-9]+\]) ]]
  
  __hcloud_cmd=mock-hcloud-new-server-new-disable-rescue-mode
  __sshkeygen_cmd=mock-cmd-error-with-message
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one
  
  run handle-new-server 'testhost'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]

  [ "${lines[0]}" == 'error: a command error message' ]
  [[ "${lines[1]}" =~ (error: host creation failed for \[testhost\] \[[0-9]+\]) ]]
  
  __hcloud_cmd=mock-hcloud-new-server-new-disable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-cmd-error
  
  run handle-new-server 'testhost'
  [ ${status} -eq 1 ]

  [[ "${lines[0]}" =~ (error: host creation failed for \[testhost\] \[[0-9]+\]) ]]
}

@test "invoke handle-new-server - error - verbose" {
  __hcloud_cmd=mock-cmd-error
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  run handle-new-server --verbose 'testhost'
  [ ${status} -eq 1 ]
  
  [ "${lines[0]}"  == 'debug: create host [testhost] ...' ]
  [[ "${lines[1]}" =~ (debug: wait until host creation has been completed for \[testhost\] \[[0-9]+\] ...) ]]
  [[ "${lines[2]}" =~ (error: host creation failed for \[testhost\] \[[0-9]+\]) ]]
  
  __hcloud_cmd=mock-hcloud-new-server-new-disable-rescue-mode
  __sshkeygen_cmd=mock-cmd-error-with-message
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one
  
  run handle-new-server --verbose 'testhost'
  [ ${status} -eq 1 ]

  [ "${lines[0]}"  == 'debug: create host [testhost] ...' ]
  [[ "${lines[1]}" =~ (debug: wait until host creation has been completed for \[testhost\] \[[0-9]+\] ...) ]]
  [ "${lines[2]}" == 'error: a command error message' ]
  [[ "${lines[3]}" =~ (error: host creation failed for \[testhost\] \[[0-9]+\]) ]]
  
  __hcloud_cmd=mock-hcloud-new-server-new-disable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-cmd-error
  
  run handle-new-server --verbose 'testhost'
  [ ${status} -eq 1 ]

  [ "${lines[0]}"  == 'debug: create host [testhost] ...' ]
  [[ "${lines[1]}" =~ (debug: wait until host creation has been completed for \[testhost\] \[[0-9]+\] ...) ]]
  [[ "${lines[2]}" =~ (error: host creation failed for \[testhost\] \[[0-9]+\]) ]]
}

#***************************
# create-hetzner-host
#***************************

@test "invoke create-hetzner-host - options only - new - disable rescue mode" {

  __hcloud_cmd=mock-hcloud-new-server-new-disable-rescue-mode-and-opts
  __sshkeygen_cmd=mock-cmd-ok-append-args-to-file
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one-and-opts

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=0
  
  local -r enableRescueMode=0
  local sshKeysList=()

  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} 0 sshKeysList "${__test_pipe_buffer}"
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  [ "${#optsList[*]}" == "${#__test_expected_callstack_args_new_disable_rescue[*]}" ]
  
  run assert-lists-equal __test_expected_callstack_args_new_disable_rescue optsList
  [ ${status} -eq 0 ]
}

@test "invoke create-hetzner-host - options only - new - enable rescue mode" {

  __hcloud_cmd=mock-hcloud-new-server-new-enable-rescue-mode-and-opts
  __sshkeygen_cmd=mock-cmd-ok-append-args-to-file
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one-and-opts

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=0
  
  local -r enableRescueMode=1
  local sshKeysList=()

  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} 0 sshKeysList "${__test_pipe_buffer}"
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  [ "${#optsList[*]}" == "${#__test_expected_callstack_args_new_enable_rescue[*]}" ]
  
  run assert-lists-equal __test_expected_callstack_args_new_enable_rescue optsList
  [ ${status} -eq 0 ]
}

@test "invoke create-hetzner-host - options only - existing - normal mode - disable rescue mode" {

  __hcloud_cmd=mock-hcloud-new-server-existing-normal-disable-rescue-mode-and-opts
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one-and-opts

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=0
  
  local -r enableRescueMode=0
  local sshKeysList=()

  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} 0 sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 0 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  [ "${#optsList[*]}" == "${#__test_expected_callstack_args_existing_normal_mode_disable_rescue[*]}" ]
  
  run assert-lists-equal __test_expected_callstack_args_existing_normal_mode_disable_rescue optsList
  [ ${status} -eq 0 ]
}

@test "invoke create-hetzner-host - options only - existing - normal mode - enable rescue mode" {

  __hcloud_cmd=mock-hcloud-new-server-existing-normal-enable-rescue-mode-and-opts
  __sshkeygen_cmd=mock-cmd-ok-append-args-to-file
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one-and-opts

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=0
  
  local -r enableRescueMode=1
  local sshKeysList=()

  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} 0 sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 0 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  [ "${#optsList[*]}" == "${#__test_expected_callstack_args_existing_normal_mode_enable_rescue[*]}" ]
  
  run assert-lists-equal __test_expected_callstack_args_existing_normal_mode_enable_rescue optsList
  [ ${status} -eq 0 ]
}

@test "invoke create-hetzner-host - options only - existing - rescue mode - disable rescue mode - error" {

  __hcloud_cmd=mock-hcloud-new-server-existing-in-recue-mode-and-opts
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one-and-opts

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=1

  local enableRescueMode=0
  local sshKeysList=()

  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} 0 sshKeysList "${__test_pipe_buffer}"
  # printf '# l [%s]\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  [ "${#optsList[*]}" == "${#__test_expected_callstack_args_existing_in_rescue_mode_error[*]}" ]
  
  run assert-lists-equal __test_expected_callstack_args_existing_in_rescue_mode_error optsList
  [ ${status} -eq 0 ]
}

@test "invoke create-hetzner-host - options only - existing - rescue mode - enable rescue mode - error" {

  __hcloud_cmd=mock-hcloud-new-server-existing-in-recue-mode-and-opts
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one-and-opts

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=1
  
  local enableRescueMode=1
  local sshKeysList=()

  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} 0 sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 1 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  [ "${#optsList[*]}" == "${#__test_expected_callstack_args_existing_in_rescue_mode_error[*]}" ]
  
  run assert-lists-equal __test_expected_callstack_args_existing_in_rescue_mode_error optsList
  [ ${status} -eq 0 ]
}

@test "invoke create-hetzner-host - new - disable rescue mode" {

  __hcloud_cmd=mock-hcloud-new-server-new-disable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=0
  
  local -r enableRescueMode=0 isVerbose=0
  local sshKeysList=()

  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} ${isVerbose} sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  local resultList=()
  read-file-buffer "${__test_pipe_buffer}" resultList
  [ "${resultList[0]}" == 'testhost|128.0.0.1|nbrg|cx11|key-user-a key-user-b key-user-c|0|0' ]
}

@test "invoke create-hetzner-host - new - disable rescue mode - verbose" {

  __hcloud_cmd=mock-hcloud-new-server-new-disable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=0
  
  local -r enableRescueMode=0 isVerbose=1
  local sshKeysList=()
  
  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} ${isVerbose} sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 0 ]
  [ "${output}" == 'debug: wait for server [testhost] to be reachable ...' ]

  resultList=()
  read-file-buffer "${__test_pipe_buffer}" resultList
  [ "${resultList[0]}" == 'testhost|128.0.0.1|nbrg|cx11|key-user-a key-user-b key-user-c|0|0' ]
}

@test "invoke create-hetzner-host - new - enable rescue mode" {

  __hcloud_cmd=mock-hcloud-new-server-new-enable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=0
  
  local -r enableRescueMode=1 isVerbose=0
  local sshKeysList=()

  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} ${isVerbose} sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  local resultList=()
  read-file-buffer "${__test_pipe_buffer}" resultList
  [ "${resultList[0]}" == 'testhost|128.0.0.1|nbrg|cx11|key-user-a key-user-b key-user-c|1|0|testpwd' ]
}

@test "invoke create-hetzner-host - new - enable rescue mode - verbose" {

  __hcloud_cmd=mock-hcloud-new-server-new-enable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=0
  
  local -r enableRescueMode=1 isVerbose=1
  local sshKeysList=()
  
  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} ${isVerbose} sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'debug: wait for server [testhost] to be reachable ...' ]
  [ "${lines[1]}" == 'debug: reboot into rescue mode and wait for server [testhost] to be reachable ...' ]

  resultList=()
  read-file-buffer "${__test_pipe_buffer}" resultList
  [ "${resultList[0]}" == 'testhost|128.0.0.1|nbrg|cx11|key-user-a key-user-b key-user-c|1|0|testpwd' ]
}

@test "invoke create-hetzner-host - existing - normal mode - disable rescue mode" {

  __hcloud_cmd=mock-hcloud-new-server-existing-normal-disable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=0
  
  local -r enableRescueMode=0 isVerbose=0
  local sshKeysList=()

  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} ${isVerbose} sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 0 ]
  [ "${output}" == 'warning: requested server [testhost] does already exist. proceeding with existing server...' ]
  
  local resultList=()
  read-file-buffer "${__test_pipe_buffer}" resultList
  [ "${resultList[0]}" == 'testhost|128.0.0.1|nbrg|cx11|key-user-a key-user-b key-user-c|0|0' ]
}

@test "invoke create-hetzner-host - existing - normal mode - disable rescue mode - verbose" {

  __hcloud_cmd=mock-hcloud-new-server-existing-normal-disable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=0
  
  local -r enableRescueMode=0 isVerbose=1
  local sshKeysList=()

  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} ${isVerbose} sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'warning: requested server [testhost] does already exist. proceeding with existing server...' ]
  [ "${lines[1]}" == 'debug: wait for server [testhost] to be reachable ...' ]

  resultList=()
  read-file-buffer "${__test_pipe_buffer}" resultList
  [ "${resultList[0]}" == 'testhost|128.0.0.1|nbrg|cx11|key-user-a key-user-b key-user-c|0|0' ]
}

@test "invoke create-hetzner-host - existing - normal mode - enable rescue mode" {

  __hcloud_cmd=mock-hcloud-new-server-existing-normal-enable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=0
  
  local -r enableRescueMode=1 isVerbose=0
  local sshKeysList=()

  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} ${isVerbose} sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 0 ]
  [ "${output}" == 'warning: requested server [testhost] does already exist. proceeding with existing server...' ]
  
  local resultList=()
  read-file-buffer "${__test_pipe_buffer}" resultList
  [ "${resultList[0]}" == 'testhost|128.0.0.1|nbrg|cx11|key-user-a key-user-b key-user-c|1|0|testpwd' ]
}

@test "invoke create-hetzner-host - existing - normal mode - enable rescue mode - verbose" {

  __hcloud_cmd=mock-hcloud-new-server-existing-normal-enable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=0
  
  local -r enableRescueMode=1 isVerbose=1
  local sshKeysList=()
  
  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} ${isVerbose} sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'warning: requested server [testhost] does already exist. proceeding with existing server...' ]
  [ "${lines[1]}" == 'debug: wait for server [testhost] to be reachable ...' ]

  resultList=()
  read-file-buffer "${__test_pipe_buffer}" resultList
  [ "${resultList[0]}" == 'testhost|128.0.0.1|nbrg|cx11|key-user-a key-user-b key-user-c|1|0|testpwd' ]
}

@test "invoke create-hetzner-host - existing - rescue mode - disable rescue mode - error" {

  __hcloud_cmd=mock-hcloud-new-server-existing-in-recue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=1
  
  local -r enableRescueMode=0 isVerbose=0
  local sshKeysList=()

  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} ${isVerbose} sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 1 ]

  [ "${lines[0]}" == 'error: requested server [testhost] does already exist and is in rescue mode already.' ]
  [ "${lines[1]}" == 'error: turn off rescue mode first and try again.' ]
}

@test "invoke create-hetzner-host - existing - rescue mode - disable rescue mode - error - verbose" {
  
  __hcloud_cmd=mock-hcloud-new-server-existing-in-recue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=1
  
  local -r enableRescueMode=0 isVerbose=1
  local sshKeysList=()

  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} ${isVerbose} sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 1 ]

  [ "${lines[0]}" == 'error: requested server [testhost] does already exist and is in rescue mode already.' ]
  [ "${lines[1]}" == 'error: turn off rescue mode first and try again.' ]
}

@test "invoke create-hetzner-host - existing - rescue mode - enable rescue mode - error" {

  __hcloud_cmd=mock-hcloud-new-server-existing-in-recue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=1
  
  local -r enableRescueMode=1 isVerbose=0
  local sshKeysList=()

  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} ${isVerbose} sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 1 ]

  [ "${lines[0]}" == 'error: requested server [testhost] does already exist and is in rescue mode already.' ]
  [ "${lines[1]}" == 'error: turn off rescue mode first and try again.' ]
}

@test "invoke create-hetzner-host - existing - rescue mode - enable rescue mode - error - verbose" {
  
  __hcloud_cmd=mock-hcloud-new-server-existing-in-recue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  # overwrite values defined in test-rmt-mocks-hcloud
  # used by mock mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __test_new_server_info_counter=1
  __test_new_server_existing=1
  __test_new_server_existing_rescue_mode=1
  
  local -r enableRescueMode=1 isVerbose=1
  local sshKeysList=()

  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} ${isVerbose} sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 1 ]

  [ "${lines[0]}" == 'error: requested server [testhost] does already exist and is in rescue mode already.' ]
  [ "${lines[1]}" == 'error: turn off rescue mode first and try again.' ]
}

@test "invoke create-hetzner-host - missing args - error" {
  run create-hetzner-host
  [ ${status} -eq 1 ]
  
  run create-hetzner-host 'testhost'
  [ ${status} -eq 1 ]
  
  run create-hetzner-host 'testhost' 'cx11'
  [ ${status} -eq 1 ]
  
  run create-hetzner-host 'testhost' 'cx11' 'd11'
  [ ${status} -eq 1 ]
  
  run create-hetzner-host 'testhost' 'cx11' 'd11' 'r'
  [ ${status} -eq 1 ]
  
  run create-hetzner-host 'testhost' 'cx11' 'd11' 'r' 0 0
  [ ${status} -eq 1 ]
  
  local keyList=()
  run create-hetzner-host 'testhost' 'cx11' 'd11' 'r' 0 0 keyList
  [ ${status} -eq 1 ]
}

@test "invoke create-hetzner-host - wrong args - error" {
  local keyList=()

  run create-hetzner-host
  [ ${status} -eq 1 ]
  
  run create-hetzner-host 'testhost' 'cxx' 'falkenstein' 'd11' 0 0 keyList 'somepipe'
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: no server-type available for key [cxx]' ]
  [ "${lines[1]}" == 'error: hetzner host type [cxx] is not supported' ]

  run create-hetzner-host 'testhost' 'cx11' 'unknown' 'd11' 0 0 keyList 'somepipe'
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: no server-location available for key [unknown]' ]
  [ "${lines[1]}" == 'error: hetzner datacenter location [unknown] is not supported' ]

  run create-hetzner-host 'testhost' 'cx11' 'random' 'alpine' 0 0 keyList 'somepipe'
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: no server-image available for key [alpine]' ]
  [ "${lines[1]}" == 'error: hetzner os image [alpine] is not supported' ]
  
  run create-hetzner-host 'testhost' 'cx11c' 'n' 'd11'
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run create-hetzner-host 'testhost' 'cx21' 'f' 'd10' 0 0
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run create-hetzner-host 'testhost' 'cx21c' 'h' 'd11' 0 0 keyList
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  local pipeName="${__test_data_dir}/somedir"
  mkdir "${pipeName}"
  run create-hetzner-host 'testhost' 'cx31c' 'falkenstein' 'd11' 0 0 keyList "${pipeName}"
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  pipeName="${__test_data_dir}/somepipe"
  run create-hetzner-host 'testhost' 'cxx' 'h' 'd11' 0 0 keyList "${pipeName}"
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'error: no server-type available for key [cxx]' ]
  [ "${lines[1]}" == 'error: hetzner host type [cxx] is not supported' ]
}

@test "invoke create-hetzner-host - error" {
  __hcloud_cmd=mock-cmd-error
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one
  
  local enableRescueMode=0 isVerbose=0
  local sshKeysList
  
  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} ${isVerbose} sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  __hcloud_cmd=mock-hcloud-new-server-new-disable-rescue-mode
  __sshkeygen_cmd=mock-cmd-error-with-message
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one
  
  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} ${isVerbose} sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: a command error message' ]
  
  __hcloud_cmd=mock-hcloud-new-server-new-disable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-cmd-error
  
  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} ${isVerbose} sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke create-hetzner-host - error - verbose" {
  __hcloud_cmd=mock-cmd-error
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one
  
  local enableRescueMode=0 isVerbose=1
  local sshKeysList
  
  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} ${isVerbose} sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  __hcloud_cmd=mock-hcloud-new-server-new-disable-rescue-mode
  __sshkeygen_cmd=mock-cmd-error-with-message
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one
  
  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} ${isVerbose} sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: a command error message' ]
  
  __hcloud_cmd=mock-hcloud-new-server-new-disable-rescue-mode
  __sshkeygen_cmd=mock-cmd-ok-no-output
  __netcat_cmd=mock-cmd-error
  
  run create-hetzner-host 'testhost' 'cx11' 'n' 'd11' ${enableRescueMode} ${isVerbose} sshKeysList "${__test_pipe_buffer}"
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}