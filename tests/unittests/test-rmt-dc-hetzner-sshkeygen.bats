#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-dc-hetzner.
#
# rmt-dc-hetzner is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-dc-hetzner is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-dc-hetzner. If not, see <https://www.gnu.org/licenses/>.

############################
# INCLUDES
############################

# currently, dotfiles to test reside one level above bats files
declare -g -r __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../../common-test/test-rmt-mocks-common"
load "${__source_dir}/../../../common-test/test-rmt-utilities"

load "${__source_dir}/../../rmt-dc-hetzner-sshkeygen.dotfile"


############################
# TEST DATA
############################

declare -r -g __test_data_dir='test-data'
# overwrite definition in test-rmt-mocks-common
__test_file_buffer="${__test_data_dir}/test-hcloud-file_buffer"

############################
# SETUP
############################

setup() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
  [[ -f "${__test_file_buffer}" ]] && rm "${__test_file_buffer}"

  mkdir "${__test_data_dir}"
}

teardown() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
}

############################
# TESTS
############################

@test "sshkeygen-remove-host-from-known-hosts - options only" {
  __sshkeygen_cmd=mock-cmd-ok-append-args-to-file

  run sshkeygen-remove-host-from-known-hosts '127.0.0.1'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList

  [ "${optsList[0]}" == '-f' ]
  [ "${optsList[1]}" == "${__rmt_dc_hetzner_known_hosts_file_default}" ]
  [ "${optsList[2]}" == '-R' ]
  [ "${optsList[3]}" == '127.0.0.1' ]
}

@test "invoke sshkeygen-remove-host-from-known-hosts" {
  __sshkeygen_cmd=mock-cmd-ok-no-output

  run sshkeygen-remove-host-from-known-hosts '127.0.0.1'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
}

@test "invoke sshkeygen-remove-host-from-known-hosts - no args" {
  __sshkeygen_cmd=mock-cmd-ok-no-output

  run sshkeygen-remove-host-from-known-hosts
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke sshkeygen-remove-host-from-known-hosts - error" {
  __sshkeygen_cmd=mock-cmd-error-with-message

  run sshkeygen-remove-host-from-known-hosts '127.0.0.1'
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: a command error message' ]
}