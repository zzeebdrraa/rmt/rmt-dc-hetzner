#!/usr/bin/env bash
# set -x

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-dc-hetzner.
#
# rmt-dc-hetzner is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-dc-hetzner is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-dc-hetzner. If not, see <https://www.gnu.org/licenses/>.

############################
# DEPENDENCIES
############################

# ./rmt-dc-hetzner.dotfile

############################
# MAIN ENTRYPOINT
############################

declare -r __rmt_dc_hetzner_sh_script_dir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
cd "${__rmt_dc_hetzner_sh_script_dir}" || exit

source ./rmt-dc-hetzner.dotfile

# see rmt-dc-hetzner.sh -h for detailed help
run-rmt-dc-hetzner "$@"
