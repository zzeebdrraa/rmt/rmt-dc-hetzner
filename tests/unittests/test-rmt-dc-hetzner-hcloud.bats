#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-dc-hetzner.
#
# rmt-dc-hetzner is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-dc-hetzner is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-dc-hetzner. If not, see <https://www.gnu.org/licenses/>.

############################
# INCLUDES
############################

# currently, dotfiles to test reside one level above bats files
declare -g -r __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../../common/math.dotfile"
load "${__source_dir}/../../../common/net.dotfile"
load "${__source_dir}/../../rmt-dc-hetzner-hcloud.dotfile"

load "${__source_dir}/../../../common-test/test-rmt-mocks-common"
load "${__source_dir}/../../../common-test/test-rmt-utilities"

load "${__source_dir}/test-rmt-mocks-hcloud"

############################
# TEST DATA
############################

declare -r -g __test_data_dir='test-data'
# overwrite definition in test-rmt-mocks-common
__test_file_buffer="${__test_data_dir}/test-hcloud-file_buffer"

############################
# SETUP
############################

setup() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
  [[ -f "${__test_file_buffer}" ]] && rm "${__test_file_buffer}"

  mkdir "${__test_data_dir}"
}

teardown() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
}

############################
# TESTS
############################

@test "invoke hcloud-get-stored-ssh-keys - only options" {

  __hcloud_cmd=mock-cmd-ok-append-args-to-file

  local listOut=()
  run hcloud-get-stored-ssh-keys listOut
  [ ${status} -eq 0 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList

  [ "${optsList[1]}" == 'list' ]
  [ "${optsList[0]}" == 'ssh-key' ]
  [ "${optsList[2]}" == '-o' ]
  [ "${optsList[3]}" == 'noheader' ]
  [ "${optsList[4]}" == '-o' ]
  [ "${optsList[5]}" == 'columns=name' ]
}

@test "invoke hcloud-get-stored-ssh-keys - with keys" {
  __hcloud_cmd=mock-hcloud-ssk-key-list-with-keys

  local res=0 listOut=()
  ! hcloud-get-stored-ssh-keys listOut && res=1
  # printf '# l %s\n' "${listOut[@]}" >&3
  [ ${res} -eq 0 ]

  [ "${listOut[0]}" == 'key-user-a' ]
  [ "${listOut[1]}" == 'key-user-b' ]
  [ "${listOut[2]}" == 'key-user-c' ]
}

@test "invoke hcloud-get-stored-ssh-keys - no keys" {
  __hcloud_cmd=mock-cmd-ok-no-output
  
  local listOut=()
  run hcloud-get-stored-ssh-keys listOut
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke hcloud-get-stored-ssh-keys - error" {
  skip "error not yet detectable"
  
  __hcloud_cmd=mock-cmd-error
  
  local listOut=()
  run hcloud-get-stored-ssh-keys listOut
  [ ${status} -eq 1 ]
  [ ${lines[*]} -eq 0 ]
}

@test "invoke hcloud-get-stored-ssh-keys - error - no args" {
  __hcloud_cmd=mock-cmd-ok-no-output
  
  local listOut=()
  run hcloud-get-stored-ssh-keys
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke hcloud-get-host-info - only options" {

  __hcloud_cmd=mock-cmd-ok-append-args-to-file

  local listOut=()
  run hcloud-get-host-info 'testhost'
  [ ${status} -eq 0 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList

  [ "${optsList[0]}" == 'server' ]
  [ "${optsList[1]}" == 'list' ]
  [ "${optsList[2]}" == '-o' ]
  [ "${optsList[3]}" == 'noheader' ]
  [ "${optsList[4]}" == '-o' ]
  [ "${optsList[5]}" == 'columns=name,ipv4,status,rescue_enabled,datacenter,type' ]
}

@test "invoke hcloud-get-host-info - matching - single host" {
  __hcloud_cmd=mock-hcloud-host-infos-single-host

  run hcloud-get-host-info 'testhost'
  [ ${status} -eq 0 ]
  [ "${output}" == '128.0.0.1|running|1|nbrg|cx11' ]
}

@test "invoke hcloud-get-host-info - matching - multiple host" {
  __hcloud_cmd=mock-hcloud-host-infos-multiple-hosts

  run hcloud-get-host-info 'testhost'
  [ ${status} -eq 0 ]
  [ "${output}" == '128.0.0.1|running|1|nbrg|cx11' ]
}

@test "invoke hcloud-get-host-info - matching - no rescue mode" {
  __hcloud_cmd=mock-hcloud-host-infos-single-host-no-rescue

  run hcloud-get-host-info 'testhost'
  [ ${status} -eq 0 ]
  [ "${output}" == '128.0.0.1|running|0|nbrg|cx11' ]
  
  __hcloud_cmd=mock-hcloud-host-infos-single-host-unknown-rescue

  run hcloud-get-host-info 'testhost'
  [ ${status} -eq 0 ]
  [ "${output}" == '128.0.0.1|running|0|nbrg|cx11' ]
  
}

@test "invoke hcloud-get-host-info - not matching" {
  __hcloud_cmd=mock-hcloud-host-infos-single-host

  run hcloud-get-host-info 'unknown-host'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke hcloud-get-host-info - error - no args" {
  __hcloud_cmd=mock-cmd-ok-no-output

  run hcloud-get-host-info
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke hcloud-get-host-info - error" {
  __hcloud_cmd=mock-cmd-error

  run hcloud-get-host-info
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke hcloud-get-active-user-context - only options" {

  __hcloud_cmd=mock-cmd-ok-append-args-to-file

  run hcloud-get-active-user-context
  # status == 1 is ok, as __hcloud_cmd does not 
  # provide any mocked user context data
  [ ${status} -eq 1 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList

  [ "${optsList[0]}" == 'context' ]
  [ "${optsList[1]}" == 'list' ]
  [ "${optsList[2]}" == '-o' ]
  [ "${optsList[3]}" == 'noheader' ]
}

@test "invoke hcloud-get-active-user-context - matching - single project" {
  __hcloud_cmd=mock-hcloud-active-user-context-single-entry

  run hcloud-get-active-user-context
  [ ${status} -eq 0 ]
  [ "${output}" == 'testuser' ]
}

@test "invoke hcloud-get-active-user-context - matching - multiple projects" {
  __hcloud_cmd=mock-hcloud-active-user-context-multiple-entries

  run hcloud-get-active-user-context
  [ ${status} -eq 0 ]
  [ "${output}" == 'testuser-project' ]

  __hcloud_cmd=mock-hcloud-active-user-context-multiple-entries-non-active

  run hcloud-get-active-user-context
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  __hcloud_cmd=mock-hcloud-active-user-context-multiple-entries-unknown-mark

  run hcloud-get-active-user-context
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke hcloud-get-active-user-context - error" {

  __hcloud_cmd=mock-cmd-error

  run hcloud-get-active-user-context
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke hcloud-enable-rescue-mode - only options" {

  __hcloud_cmd=mock-cmd-ok-append-args-to-file

  run hcloud-enable-rescue-mode 'testhost'
  # status==1 is ok, as hcloud mock does not print expected message
  [ ${status} -eq 1 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList

  [ "${optsList[0]}" == 'server' ]
  [ "${optsList[1]}" == 'enable-rescue' ]
  [ "${optsList[2]}" == 'testhost' ]
}

@test "invoke hcloud-enable-rescue-mode" {

  __hcloud_cmd=mock-hcloud-enable-rescue-mode-root-pwd

  run hcloud-enable-rescue-mode 'testhost'
  [ ${status} -eq 0 ]
  [ "${output}" == 'testpwd' ]
}

@test "invoke hcloud-enable-rescue-mode - error" {

  __hcloud_cmd=mock-cmd-error

  run hcloud-enable-rescue-mode 'testhost'
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke hcloud-enable-rescue-mode - error - no args" {

  __hcloud_cmd=mock-cmd-ok-no-output

  run hcloud-enable-rescue-mode
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke hcloud-remove-server - only options" {

  __hcloud_cmd=mock-hcloud-remove-server-list-available-and-opts

  run hcloud-remove-server 0 0 'testhost'
  [ ${status} -eq 0 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  rm "${__test_file_buffer}"
  
  [ "${optsList[0]}" == 'server' ]
  [ "${optsList[1]}" == 'list' ]
  [ "${optsList[2]}" == '-o' ]
  [ "${optsList[3]}" == 'columns=name' ]
  [ "${optsList[4]}" == '-o' ]
  [ "${optsList[5]}" == 'noheader' ]
  [ "${optsList[6]}" == 'server' ]
  [ "${optsList[7]}" == 'delete' ]
  [ "${optsList[8]}" == 'testhost' ]

  # dry-run
  run hcloud-remove-server 0 1 'testhost'
  [ ${status} -eq 0 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList

  [ "${optsList[0]}" == 'server' ]
  [ "${optsList[1]}" == 'list' ]
  [ "${optsList[2]}" == '-o' ]
  [ "${optsList[3]}" == 'columns=name' ]
  [ "${optsList[4]}" == '-o' ]
  [ "${optsList[5]}" == 'noheader' ]
  [ "${#optsList[*]}" -eq 6 ]
}

@test "invoke hcloud-remove-server - match" {
  __hcloud_cmd=mock-hcloud-remove-server-list-available

  run hcloud-remove-server 0 0 'testhost'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run hcloud-remove-server 1 0 'testhost'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ "${output}" == 'debug: delete hetzner host [testhost]' ]
  
  run hcloud-remove-server 0 1 'testhost'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ "${output}" == 'dry-run: hcloud server delete testhost' ]
}

@test "invoke hcloud-remove-server - no match" {
  __hcloud_cmd=mock-hcloud-remove-server-list-available

  run hcloud-remove-server 0 0 'unknownhost'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run hcloud-remove-server 1 0 'unknownhost'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run hcloud-remove-server 0 1 'unknownhost'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke hcloud-remove-server - partly match" {
  __hcloud_cmd=mock-hcloud-remove-server-list-available-single-failure

  run hcloud-remove-server 0 0 'localhost' 'testhost' 'examplehost'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: could not delete hetzner host [localhost]' ]
  
  run hcloud-remove-server 1 0 'localhost' 'testhost' 'examplehost'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  [ "${lines[0]}" == 'debug: delete hetzner host [testhost]' ]
  [ "${lines[1]}" == 'debug: delete hetzner host [examplehost]' ]
  [ "${lines[2]}" == 'debug: delete hetzner host [localhost]' ]
  [ "${lines[3]}" == 'error: could not delete hetzner host [localhost]' ]
}

@test "invoke hcloud-remove-server - error" {

  __hcloud_cmd=mock-hcloud-remove-server-list-available-and-error

  run hcloud-remove-server 0 0 'testhost'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 1 ]
  [ "${output}" == 'error: could not delete hetzner host [testhost]' ]
}

@test "invoke hcloud-remove-server - error - no args" {

  __hcloud_cmd=mock-cmd-ok-no-output

  run hcloud-remove-server
  [ ${status} -eq 1 ]
  # [ -z "${output}" ]
  
  run hcloud-remove-server 0
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run hcloud-remove-server 0 0
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke hcloud-create-server - only options" {

  __hcloud_cmd=mock-cmd-ok-append-args-to-file

  local keyList=()
  run hcloud-create-server 0 'testhost' 'cx11' 'nbrg' 'debian11' keyList
  [ ${status} -eq 0 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  rm "${__test_file_buffer}"

  [ "${optsList[0]}" == 'server' ]
  [ "${optsList[1]}" == 'create' ]
  [ "${optsList[2]}" == '--name' ]
  [ "${optsList[3]}" == 'testhost' ]
  [ "${optsList[4]}" == '--type' ]
  [ "${optsList[5]}" == 'cx11' ]
  [ "${optsList[6]}" == '--datacenter' ]
  [ "${optsList[7]}" == 'nbrg' ]
  [ "${optsList[8]}" == '--image' ]
  [ "${optsList[9]}" == 'debian11' ]
  
  keyList=('keya' 'keyb' 'keyc')
  run hcloud-create-server 0 'testhost' 'cx11' 'nbrg' 'debian11' keyList
  [ ${status} -eq 0 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList

  [ "${optsList[0]}" == 'server' ]
  [ "${optsList[1]}" == 'create' ]
  [ "${optsList[2]}" == '--name' ]
  [ "${optsList[3]}" == 'testhost' ]
  [ "${optsList[4]}" == '--type' ]
  [ "${optsList[5]}" == 'cx11' ]
  [ "${optsList[6]}" == '--datacenter' ]
  [ "${optsList[7]}" == 'nbrg' ]
  [ "${optsList[8]}" == '--image' ]
  [ "${optsList[9]}" == 'debian11' ]
  [ "${optsList[10]}" == '--ssh-key' ]
  [ "${optsList[11]}" == 'keya' ]
  [ "${optsList[12]}" == '--ssh-key' ]
  [ "${optsList[13]}" == 'keyb' ]
  [ "${optsList[14]}" == '--ssh-key' ]
  [ "${optsList[15]}" == 'keyc' ]
}

@test "invoke hcloud-create-server - new host" {

  __hcloud_cmd=mock-hcloud-create-server

  local keyList=()
  run hcloud-create-server 0 'testhost' 'cx11' 'nbrg' 'debian11' keyList
  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  run hcloud-create-server 1 'testhost' 'cx11' 'nbrg' 'debian11' keyList
  [ ${status} -eq 0 ]
  [ "${output}" == 'created server testhost' ]
}

@test "invoke hcloud-create-server - error" {

  __hcloud_cmd=mock-cmd-error

  local keyList=()
  run hcloud-create-server 0 'testhost' 'cx11' 'nbrg' 'debian11' keyList

  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke hcloud-create-server - error - no args" {

  __hcloud_cmd=mock-cmd-ok-no-output

  run hcloud-create-server
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run hcloud-create-server 0
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run hcloud-create-server 0 'testhost' 'cx11'
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run hcloud-create-server 0 'testhost' 'cx11' 'nbrg' 
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run hcloud-create-server 0 'testhost' 'cx11' 'nbrg' 'debian11' 
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke hcloud-reboot-server-and-wait - only options" {

  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one-and-opts
  __hcloud_cmd=mock-hcloud-reboot-server-and-netcat-try-one-and-opts

  run hcloud-reboot-server-and-wait 'testhost' '127.0.0.0' '22' '1' '1'
  # printf '# l %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  rm "${__test_file_buffer}"

  [ "${optsList[0]}" == 'server' ]
  [ "${optsList[1]}" == 'poweroff' ]
  [ "${optsList[2]}" == 'testhost' ]
  [ "${optsList[3]}" == '-w' ]
  [ "${optsList[4]}" == '1' ]
  [ "${optsList[5]}" == '-zn' ]
  [ "${optsList[6]}" == '127.0.0.0' ]
  [ "${optsList[7]}" == '22' ]
  [ "${optsList[8]}" == 'server' ]
  [ "${optsList[9]}" == 'poweron' ]
  [ "${optsList[10]}" == 'testhost' ]
  [ "${optsList[11]}" == '-w' ]
  [ "${optsList[12]}" == '1' ]
  [ "${optsList[13]}" == '-zn' ]
  [ "${optsList[14]}" == '127.0.0.0' ]
  [ "${optsList[15]}" == '22' ]
}

@test "invoke hcloud-reboot-server-and-wait" {

  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-one
  __hcloud_cmd=mock-hcloud-reboot-server-and-netcat-try-one

  run hcloud-reboot-server-and-wait 'testhost' '127.0.0.0' '22' '1' '1'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-two-three

  run hcloud-reboot-server-and-wait 'testhost' '127.0.0.0' '22' '3' '1'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke hcloud-reboot-server-and-wait - timeout" {
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-two-three
  __hcloud_cmd=mock-hcloud-reboot-server-and-netcat-try-two-three

  run hcloud-reboot-server-and-wait 'testhost' '127.0.0.0' '22' '2' '1'
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  __netcat_cmd=mock-hcloud-reboot-server-and-netcat-try-three-two
  __hcloud_cmd=mock-hcloud-reboot-server-and-netcat-try-three-two

  run hcloud-reboot-server-and-wait 'testhost' '127.0.0.0' '22' '2' '1'
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke hcloud-reboot-server-and-wait - error" {

  __netcat_cmd=mock-netcat-try-one
  __hcloud_cmd=mock-hcloud-reboot-server-poweroff-error

  run hcloud-reboot-server-and-wait 'testhost' '127.0.0.0' '22' '1' '1'
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  __netcat_cmd=mock-netcat-try-one
  __hcloud_cmd=mock-hcloud-reboot-server-poweron-error

  run hcloud-reboot-server-and-wait 'testhost' '127.0.0.0' '22' '1' '1'
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke hcloud-reboot-server-and-wait - no args" {

  __netcat_cmd=mock-cmd-ok-no-output
  __hcloud_cmd=mock-cmd-ok-no-output

  run hcloud-reboot-server-and-wait
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run hcloud-reboot-server-and-wait 'testhost'
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run hcloud-reboot-server-and-wait 'testhost' '127.0.0.0'
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run hcloud-reboot-server-and-wait 'testhost' '127.0.0.0' '22'
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
  
  run hcloud-reboot-server-and-wait 'testhost' '127.0.0.0' '22' '1'
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke hcloud-list-server - only options" {

  __hcloud_cmd=mock-cmd-ok-append-args-to-file

  run hcloud-list-server 0 'testhost'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  rm "${__test_file_buffer}"

  [ "${optsList[0]}" == 'server' ]
  [ "${optsList[1]}" == 'list' ]
  [ "${optsList[2]}" == '-o' ]
  [ "${optsList[3]}" == 'noheader' ]
}

@test "invoke hcloud-list-server" {
  __hcloud_cmd=mock-hcloud-list-server-one

  run hcloud-list-server 0 'testhost'
  [ ${status} -eq 0 ]
  [ "${output}" == '34gf76sde54 testhost 127.0.0.1' ]
  
  __hcloud_cmd=mock-hcloud-list-server-three

  run hcloud-list-server 0 'host'
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '34gf76sde54 localhost 127.0.0.1' ]
  [ "${lines[1]}" == '34gf76sde55 testhost 127.0.0.2' ]
  [ "${lines[2]}" == '34gf76sde56 examplehost 127.0.0.3' ]
  
  run hcloud-list-server 0 'example'
  [ ${status} -eq 0 ]
  [ "${output}" == '34gf76sde56 examplehost 127.0.0.3' ]
  
  run hcloud-list-server 0 'local' 'test'
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '34gf76sde54 localhost 127.0.0.1' ]
  [ "${lines[1]}" == '34gf76sde55 testhost 127.0.0.2' ]
}

@test "invoke hcloud-list-server - no match" {
  __hcloud_cmd=mock-hcloud-list-server-three

  run hcloud-list-server 0 'unknown'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run hcloud-list-server 1 'unknown'
  [ ${status} -eq 0 ]
  [ "${output}" == 'debug: no running servers' ]
  
  __hcloud_cmd=mock-cmd-ok-no-output

  run hcloud-list-server 0 'testhost'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run hcloud-list-server 1 'testhost'
  [ ${status} -eq 0 ]
  [ "${output}" == 'debug: no running servers' ]
}

@test "invoke hcloud-list-server - error" {
  skip 'error not yet detectable'
  __hcloud_cmd=mock-cmd-error

  run hcloud-list-server 0 'testhost'
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke hcloud-list-server-ip - only options" {
  __hcloud_cmd=mock-cmd-ok-append-args-to-file

  run hcloud-list-server-ip 0 'ipv4' 'testhost'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  rm "${__test_file_buffer}"

  [ "${optsList[0]}" == 'server' ]
  [ "${optsList[1]}" == 'list' ]
  [ "${optsList[2]}" == '-o' ]
  [ "${optsList[3]}" == 'columns=name,ipv4' ]
  [ "${optsList[4]}" == '-o' ]
  [ "${optsList[5]}" == 'noheader' ]
  
  __hcloud_cmd=mock-cmd-ok-append-args-to-file

  run hcloud-list-server-ip 0 'ipv6' 'testhost'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  rm "${__test_file_buffer}"

  [ "${optsList[0]}" == 'server' ]
  [ "${optsList[1]}" == 'list' ]
  [ "${optsList[2]}" == '-o' ]
  [ "${optsList[3]}" == 'columns=name,ipv6' ]
  [ "${optsList[4]}" == '-o' ]
  [ "${optsList[5]}" == 'noheader' ]
}

@test "invoke hcloud-list-server-ip" {
  __hcloud_cmd=mock-hcloud-list-server-ip-one

  run hcloud-list-server-ip 0 'ipv4' 'testhost'
  [ ${status} -eq 0 ]
  [ "${output}" == '127.0.0.1' ]

  run hcloud-list-server-ip 0 'ipv6' 'testhost'
  [ ${status} -eq 0 ]
  [ "${output}" == '2001:4dd0:100:1020:53:2::2' ]

  __hcloud_cmd=mock-hcloud-list-server-ip-three

  run hcloud-list-server-ip 0 'ipv4' 'host'
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '127.0.0.1' ]
  [ "${lines[1]}" == '127.0.0.2' ]
  [ "${lines[2]}" == '127.0.0.3' ]
  
  run hcloud-list-server-ip 0 'ipv4' 'example'
  [ ${status} -eq 0 ]
  [ "${output}" == '127.0.0.3' ]
  
  run hcloud-list-server-ip 0 'ipv4' 'local' 'test'
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '127.0.0.1' ]
  [ "${lines[1]}" == '127.0.0.2' ]
  
  run hcloud-list-server-ip 0 'ipv6' 'host'
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '2001:4dd0:100:1020:53:2::1' ]
  [ "${lines[1]}" == '2001:4dd0:100:1020:53:2::2' ]
  [ "${lines[2]}" == '2001:4dd0:100:1020:53:2::3' ]
  
  run hcloud-list-server-ip 0 'ipv6' 'example'
  [ ${status} -eq 0 ]
  [ "${output}" == '2001:4dd0:100:1020:53:2::3' ]
  
  run hcloud-list-server-ip 0 'ipv6' 'local' 'test'
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '2001:4dd0:100:1020:53:2::1' ]
  [ "${lines[1]}" == '2001:4dd0:100:1020:53:2::2' ]
}

@test "invoke hcloud-list-server-ip - no match" {
  __hcloud_cmd=mock-hcloud-list-server-ip-three

  run hcloud-list-server-ip 0 'ipv4' 'unknown'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run hcloud-list-server-ip 1 'ipv4' 'unknown'
  [ ${status} -eq 0 ]
  [ "${output}" == 'debug: no running servers' ]
  
  run hcloud-list-server-ip 0 'ipv6' 'unknown'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run hcloud-list-server-ip 1 'ipv6' 'unknown'
  [ ${status} -eq 0 ]
  [ "${output}" == 'debug: no running servers' ]
  
  __hcloud_cmd=mock-cmd-ok-no-output

  run hcloud-list-server-ip 0 'ipv4' 'testhost'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run hcloud-list-server-ip 1 'ipv4' 'testhost'
  [ ${status} -eq 0 ]
  [ "${output}" == 'debug: no running servers' ]
  
  run hcloud-list-server-ip 0 'ipv6' 'testhost'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run hcloud-list-server-ip 1 'ipv6' 'testhost'
  [ ${status} -eq 0 ]
  [ "${output}" == 'debug: no running servers' ]
}

@test "invoke hcloud-list-server-ip - error" {
  skip 'error not yet detectable'
  __hcloud_cmd=mock-cmd-error

  run hcloud-list-server-ip 0 'ipv4' 'testhost'
  [ ${status} -eq 1 ]
  [ -z "${output}" ]

  run hcloud-list-server-ip 0 'ipv6' 'testhost'
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}