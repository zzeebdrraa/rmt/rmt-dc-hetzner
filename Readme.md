# rmt-dc-hetzner

`rmt-dc-hetzner` is a command line tool that books or removes one or more host(s) in a Hetzner datacenter.

it wraps around the `hcloud` tool for controlling Hetzner hosts and just adds some bulk creation and removal features.

## Note

*this project is currently work in progress. once end-2-end tests have been implemented and ran successfully, it is safe to use.*

## Features

- book one or more servers in a Hetzner datacenter
- list all running servers or filter running servers by name patterns
- remove one or more servers, either by fullname or name patterns

## Dependencies

- `ssh-keygen`
- `mkfifo`
- `mktemp`
- `nc`
- [hcloud](https://github.com/hetznercloud/cli)
- [bats (for unit testing)](https://github.com/bats-core/bats-core)

## Installation

TO BE DONE

## Testing

```bash
cd tests/unittests
# run all bats tests in this directory
bats .
```

## Submodules

submodules for acessing library code are not in place yet. currently, one should manually checkout the following repos alongside this repo:

- common-test
- common

## Prerequisits

- it is necessary to have a personal account at Hetzner
- the hcloud tool has to be configured for authentication and to point to a personal Hetzner workspace, where servers are about to be added to or removed

## Note

when creating new hosts in a Hetzer datacenter, expenses are incurred.

## Help

For detailed help run

```bash
rmt-dc-hetzner.sh help
```

## Usage

```bash
rmt-dc-hetzner.sh -h|--help

rmt-dc-hetzner.sh h|help
rmt-dc-hetzner.sh h|help s|short
rmt-dc-hetzner.sh h|help d|details
rmt-dc-hetzner.sh h|help e|examples
  
rmt-dc-hetzner.sh --version

rmt-dc-hetzner.sh i|info 
                ([l|location] [i|image] [t|type])

rmt-dc-hetzner.sh ip4|list-ip4
                [-v|--verbose]
                (<host-name-regex>)

rmt-dc-hetzner.sh ip6|list-ip6
                [-v|--verbose]
                (<host-name-regex>)

rmt-dc-hetzner.sh ls|list
                [-v|--verbose]
                (<host-name-regex>)

rmt-dc-hetzner.sh n|new
                [-s|--silent | -v|--verbose | --kv|--key-value] 
                [--re|--enable-rescue-mode]
                [-l|--location <datacenter>]
                [-t|--type <host-type>]
                [-i|--image <imagename>]
                [-k|--key-to-add <keyname>]
                <new-host-name-list>

rmt-dc-hetzner.sh rm|remove
                [-v|--verbose]
                [-d|--dry-run]
                <host-name-regex>

rmt-dc-hetzner.sh s|status
```

## Exit codes

- 0 - host creation was sucessfull, all hosts are available
- 1 - host creation failed completely, no host is available
- 2 - host creation failed partly, some host are available, some not

## Examples

```bash
# create a new Hetzner host of type [CX22] 
# install OS [debian-11]
# and ssh-keys available in Hetzner backend
# host is booted into rescue-mode 

# default behaviour: print host-name, host-ip and rescue mode password to stdout
rmt-dc-hetzner.sh new --enable-rescue-mode host-dev
> host-dev 159.69.111.74 tRnuWKRmEsrum4AxTTT7
  
# explicit: print host-name, host-ip and rescue mode password to stdout
rmt-dc-hetzner.sh new --key-value --enable-rescue-mode host-dev
> host-dev 159.69.111.74 tRnuWKRmEsrum4AxTTT7
  
# do not print anything to stdout, except errors to stderr
rmt-dc-hetzner.sh new --silent --enable-rescue-mode  host-dev
  
# print debug messages and errors to stdout/stderr
rmt-dc-hetzner.sh new --verbose --enable-rescue-mode host-dev
>hetzner server:
>--------------
>name        : host-dev
>ip          : 159.69.111.74
>type        : CX22
>location    : Helsinki
>rescue mode : 1
>ssh-auth    : userA,userB
>--------------
```

```bash
# create a new Hetzner host of type [CX22] 
# install OS [debian-11]
# and ssh-keys available in Hetzner backend
# created host is kept in normal mode (no reboot into rescue mode)  

# default behaviour: print host-name and host-ip to stdout
rmt-dc-hetzner.sh new host-dev
> host-dev 159.69.111.74
  
# create several hosts at once
rmt-dc-hetzner.sh new host-dev-1 host-dev-2 host-dev-3
> host-dev-1 159.69.111.74
> host-dev-2 159.69.111.75
> host-dev-3 159.69.111.76
```

```bash
# choose datacenter, server type, etc  
  
# create a new Hetzner host of type [ccx51] in Hetzner datacenter in [Helsinki]
# install OS [ubuntu-18.04] and ssh-keys for [userA userB]
rmt-dc-hetzner.sh new --verbose -i ubuntu-18.04 -l helsinki -t ccx51 -k userA -k userB host-prod
>hetzner server:
>--------------
>name        : host-prod
>ip          : 159.69.111.76
>type        : CCX51
>location    : Helsinki
>rescue mode : 0
>ssh-auth    : userA,userB
>--------------
```

```bash
print infos about script options

# print all infos
rmt-dc-hetzner.sh info
  
# only print infos about server types
rmt-dc-hetzner.sh info type
  
# print infos about datacenter location and os-images
rmt-dc-hetzner.sh info image location
```

```bash
# print status of mandatory dependencies available on local machine
rmt-dc-hetzner.sh status
> hcloud is /usr/local/bin/hcloud
> ssh-keygen is /usr/bin/ssh-keygen
> mkfifo is /usr/bin/mkfifo
> mktemp is /usr/bin/mktemp
> nc is /usr/bin/nc
```

## ToDo

- create git submodules
- install via `make`
- run tests via `make`
- extract testing taxonomy in order to create a generic taxonomy for future testcases
- run code coverage of bats tests with [bashcov](https://github.com/infertux/bashcov)
- implement end-to-end tests with [robotframework](https://github.com/robotframework/robotframework)

## License

[GPL3](https://www.gnu.org/licenses/gpl-3.0.html#license-text)
