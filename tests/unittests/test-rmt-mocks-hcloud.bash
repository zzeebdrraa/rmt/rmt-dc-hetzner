#!/usr/bin/env bash

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-dc-hetzner.
#
# rmt-dc-hetzner is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-dc-hetzner is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-dc-hetzner. If not, see <https://www.gnu.org/licenses/>.

############################
# DEPENDENCIES
############################

# the following file must have been sourced before using functions of this file:
#
# ../../common-test/test-rmt-mocks-common.bash

############################
# MOCKS
############################

mock-hcloud-ssk-key-list-with-keys() {
  printf '%s\n%s\n%s\n' 'key-user-a' 'key-user-b' 'key-user-c'
  return 0
}

mock-hcloud-host-infos-single-host() {
  printf '%s %s %s %s %s %s\n' 'testhost' '128.0.0.1' 'running' 'yes' 'nbrg' 'cx11'
  return 0
}

mock-hcloud-host-infos-single-host-no-rescue() {
  printf '%s %s %s %s %s %s\n' 'testhost' '128.0.0.1' 'running' 'no' 'nbrg' 'cx11'
  return 0
}

mock-hcloud-host-infos-single-host-unknown-rescue() {
  printf '%s %s %s %s %s %s\n' 'testhost' '128.0.0.1' 'running' 'what' 'nbrg' 'cx11'
  return 0
}

mock-hcloud-host-infos-multiple-hosts() {
  printf '%s %s %s %s %s %s\n' 'localhost' '0.0.0.0' 'running' 'yes' 'hels' 'cx33'
  printf '%s %s %s %s %s %s\n' 'examplehost' '169.168.0.1' 'shutdown' 'no' 'hels' 'cx22'
  printf '%s %s %s %s %s %s\n' 'testhost' '128.0.0.1' 'running' 'yes' 'nbrg' 'cx11'
  return 0
}

mock-hcloud-active-user-context-single-entry() {
  printf '%s %s\n' '*' 'testuser'
  return 0
}

mock-hcloud-active-user-context-multiple-entries() {
  printf '%s %s\n' '' 'unknown-project'
  printf '%s %s\n' '' 'outdated-project'
  printf '%s %s\n' '*' 'testuser-project'
  return 0
}

mock-hcloud-active-user-context-multiple-entries-non-active() {
  printf '%s %s\n' '' 'unknown-project'
  printf '%s %s\n' '' 'outdated-project'
  printf '%s %s\n' '' 'testuser-project'
  return 0
}

mock-hcloud-active-user-context-multiple-entries-unknown-mark() {
  printf '%s %s\n' '' 'unknown-project'
  printf '%s %s\n' '' 'outdated-project'
  printf '%s %s\n' '-' 'testuser-project'
  return 0
}

mock-hcloud-enable-rescue-mode-root-pwd() {
  printf 'Rescue enabled for server %s with root password: %s\n' 'cd34rf21gf' 'testpwd'
  return 0
}

mock-hcloud-remove-server-list-available() {
  [[ "$1" == 'server' ]] && [[ "$2" == 'list' ]] && printf '%s\n%s\n%s\n' 'testhost' 'examplehost' 'localhost'
  return 0
}

mock-hcloud-remove-server-list-available-single-failure() {
  mock-hcloud-remove-server-list-available "$@"
  [[ "$1" == 'server' ]] && [[ "$2" == 'delete' ]] && [[ "$3" == 'localhost' ]] && return 1
  return 0
}

mock-hcloud-remove-server-list-available-and-opts() {
  mock-cmd-ok-append-args-to-file "$@"
  mock-hcloud-remove-server-list-available "$@"
  return 0
}

mock-hcloud-remove-server-list-available-and-error() {
  mock-hcloud-remove-server-list-available "$@"
  return 1
}

mock-hcloud-create-server() {
  printf 'created server %s\n' "$4"
  return 0
}

mock-hcloud-reboot-server-poweroff-error() {
  case "$2" in
    poweroff)
      return 1
      ;;
    poweron)
      return 0
      ;;
  esac
  return 1
}

mock-hcloud-reboot-server-poweron-error() {
  case "$2" in
    poweroff)
      return 0
      ;;
    poweron)
      return 1
      ;;
  esac
  return 1
}

mock-hcloud-list-server-one() {
  printf '%s %s %s\n' '34gf76sde54' 'testhost' '127.0.0.1'
  return 0
}

mock-hcloud-list-server-three() {
  printf '%s %s %s\n' '34gf76sde54' 'localhost' '127.0.0.1'
  printf '%s %s %s\n' '34gf76sde55' 'testhost' '127.0.0.2'
  printf '%s %s %s\n' '34gf76sde56' 'examplehost' '127.0.0.3'
  return 0
}

mock-hcloud-list-server-three-and-active-user-context() {
  case "$1" in
    context)
      mock-hcloud-active-user-context-single-entry
      return 0
      ;;
    server)
      mock-hcloud-list-server-three
      return 1
      ;;
  esac
  return 1
}

mock-hcloud-list-server-ip-one() {
  if [[ "$4" =~ ipv4 ]]; then
    printf '%s %s\n' 'testhost' '127.0.0.1'
  elif [[ "$4" =~ ipv6 ]]; then
    printf '%s %s\n' 'testhost' '2001:4dd0:100:1020:53:2::2'
  else
    return 0
  fi
  return 0
}

mock-hcloud-list-server-ip-three() {
  if [[ "$4" =~ ipv4 ]]; then
    printf '%s %s\n' 'localhost' '127.0.0.1'
    printf '%s %s\n' 'testhost' '127.0.0.2'
    printf '%s %s\n' 'examplehost' '127.0.0.3'
  elif [[ "$4" =~ ipv6 ]]; then
    printf '%s %s\n' 'localhost' '2001:4dd0:100:1020:53:2::1'
    printf '%s %s\n' 'testhost' '2001:4dd0:100:1020:53:2::2'
    printf '%s %s\n' 'examplehost' '2001:4dd0:100:1020:53:2::3'
  else
    return 1
  fi
  return 0
}

mock-hcloud-list-server-ip-three-and-active-user-context() {
  # printf '# o %s\n' "$@" >&3
  case "$1" in
    context)
      mock-hcloud-active-user-context-single-entry
      return 0
      ;;
    server)
      mock-hcloud-list-server-ip-three "$@"
      return 1
      ;;
  esac
  return 1
}

############################
# MOCKS FOR NEW SERVER COMMAND
############################

mock-hcloud-new-server-errors() {
  return 1
}

declare -g __test_new_server_info_counter
declare -g __test_new_server_existing
declare -g __test_new_server_existing_rescue_mode
declare -g __test_new_server_info_counter_buffer='test-new-server-info-counter-buffer'

declare -g -a __mock_hcloud_new_server_callstack_new_disable_rescue_mode=(
  'mock-hcloud-ssk-key-list-with-keys'
  'mock-cmd-ok-no-output'
  'mock-cmd-ok-no-output'
  'mock-hcloud-host-infos-single-host-no-rescue'
)

declare -g -a __mock_hcloud_new_server_callstack_new_enable_rescue_mode=(
  'mock-hcloud-ssk-key-list-with-keys'
  'mock-cmd-ok-no-output'
  'mock-cmd-ok-no-output'
  'mock-hcloud-host-infos-single-host-no-rescue'
  'mock-hcloud-enable-rescue-mode-root-pwd'
  'mock-hcloud-reboot-hcloud-mode poweroff ; mock-hcloud-reboot-netcat-mode 1 1'
  'mock-hcloud-reboot-hcloud-mode poweron ; mock-hcloud-reboot-netcat-mode 1 1'
)

declare -g -a __mock_hcloud_new_server_callstack_existing_normal_disable_rescue_mode=(
  'mock-hcloud-ssk-key-list-with-keys'
  'mock-hcloud-host-infos-single-host-no-rescue'
)

declare -g -a __mock_hcloud_new_server_callstack_existing_normal_enable_rescue_mode=(
  'mock-hcloud-ssk-key-list-with-keys'
  'mock-hcloud-host-infos-single-host-no-rescue'
  'mock-hcloud-enable-rescue-mode-root-pwd'
  'mock-hcloud-reboot-hcloud-mode poweroff ; mock-hcloud-reboot-netcat-mode 1 1'
  'mock-hcloud-reboot-hcloud-mode poweron ; mock-hcloud-reboot-netcat-mode 1 1'
)

declare -g -a __mock_hcloud_new_server_callstack_existing_in_rescue_mode=(
  'mock-hcloud-ssk-key-list-with-keys'
  'mock-hcloud-host-infos-single-host'
)

# the mock to be used to substitte hcloud command when running server-new tests
# this mock returns the necessary data for each hcloud command called during
# server creation
#
# before calling this mock, set the following variables in your test
#
# __test_new_server_existing => 0 or 1
# __test_new_server_existing_rescue_mode => 0 or 1 (if __test_new_server_existing==1)
#
# you may also overwrite the location in 
# __test_new_server_info_counter_buffer
#

mock-hcloud-new-server-new-disable-rescue-mode-and-opts() {
  mock-cmd-ok-append-args-to-file "$@"
  mock-hcloud-new-server-new-disable-rescue-mode
}

mock-hcloud-new-server-new-enable-rescue-mode-and-opts() {
  mock-cmd-ok-append-args-to-file "$@"
  mock-hcloud-new-server-new-enable-rescue-mode
}

mock-hcloud-new-server-existing-normal-disable-rescue-mode-and-opts() {
  mock-cmd-ok-append-args-to-file "$@"
  mock-hcloud-new-server-existing-normal-disable-rescue-mode
}

mock-hcloud-new-server-existing-normal-enable-rescue-mode-and-opts() {
  mock-cmd-ok-append-args-to-file "$@"
  mock-hcloud-new-server-existing-normal-enable-rescue-mode
}

mock-hcloud-new-server-existing-in-recue-mode-and-opts() {
  mock-cmd-ok-append-args-to-file "$@"
  mock-hcloud-new-server-existing-in-recue-mode
}

mock-hcloud-new-server-new-disable-rescue-mode() {
  mock-hcloud-new-server __mock_hcloud_new_server_callstack_new_disable_rescue_mode
}

mock-hcloud-new-server-new-enable-rescue-mode() {
  mock-hcloud-new-server __mock_hcloud_new_server_callstack_new_enable_rescue_mode
}

mock-hcloud-new-server-existing-normal-disable-rescue-mode() {
  mock-hcloud-new-server __mock_hcloud_new_server_callstack_existing_normal_disable_rescue_mode
}

mock-hcloud-new-server-existing-normal-enable-rescue-mode() {
  mock-hcloud-new-server __mock_hcloud_new_server_callstack_existing_normal_enable_rescue_mode
}

mock-hcloud-new-server-existing-in-recue-mode() {
  mock-hcloud-new-server __mock_hcloud_new_server_callstack_existing_in_rescue_mode
}

mock-hcloud-new-server() {
  declare -n callstackListRef="$1"
# set -x
  [[ -f "${__test_new_server_info_counter_buffer}" ]] && {
    read -r __test_new_server_info_counter <"${__test_new_server_info_counter_buffer}"
  }
  [[ -z "${__test_new_server_info_counter}" ]] && __test_new_server_info_counter=1
  [[ -z "${__test_new_server_existing}" ]] && __test_new_server_existing=0

  local cmd=${callstackListRef[$((__test_new_server_info_counter-1))]}
  [[ -z "${cmd}" ]] && return 1
  ${cmd}
  # printf '# c %s\n' "${cmd}" >&3
  
  __test_new_server_info_counter=$((__test_new_server_info_counter+1))
  printf '%s\n' "${__test_new_server_info_counter}" >"${__test_new_server_info_counter_buffer}"
  return 0
}

############################
# MOCKS FOR NETCAT
############################

declare -g __try_counter=1
declare -g __server_poweroff=-1

mock-hcloud-reboot-server-and-netcat-try-one() {
  if [[ "$1" == 'server' ]]; then
    mock-hcloud-reboot-hcloud-mode "$2"
    return
  else
    mock-hcloud-reboot-netcat-mode 1 1
    return
  fi
  return 2
}

mock-hcloud-reboot-server-and-netcat-try-two-three() {
  if [[ "$1" == 'server' ]]; then
    mock-hcloud-reboot-hcloud-mode "$2"
    return
  else
    mock-hcloud-reboot-netcat-mode 1 3
    return
  fi
  return 2
}

mock-hcloud-reboot-server-and-netcat-try-three-two() {
  if [[ "$1" == 'server' ]]; then
    mock-hcloud-reboot-hcloud-mode "$2"
    return
  else
    mock-hcloud-reboot-netcat-mode 3 1
    return
  fi
  return 2
}

# NOTE uses __test_file_buffer defined in test-rmt-mocks-common.bash
mock-hcloud-reboot-server-and-netcat-try-one-and-opts() {
  printf '%s\n' "$@" >>"${__test_file_buffer}"
  mock-hcloud-reboot-server-and-netcat-try-one "$@"
  return
}

mock-netcat-try-one() {
  return 0
}

mock-hcloud-reboot-hcloud-mode() {
  case "$1" in
    poweroff)
      __server_poweroff=1
      ;;
    poweron)
      __server_poweroff=0
      ;;
    *)
      return 1
  esac
  return 0
}

mock-hcloud-reboot-netcat-mode() {
  local -r counterRetryPowerOn="$1"
  local -r counterRetryPowerOff="$2"

   # netcat mode
  if [[ ${__server_poweroff} -eq 0 ]]; then
    [[ ${__try_counter} -lt ${counterRetryPowerOn} ]] && {
      __try_counter=$((__try_counter+1))
      return 1
    }
    # reset
    __try_counter=1
    __server_poweroff=-1
    return 0
  elif [[ ${__server_poweroff} -eq 1 ]]; then
    [[ ${__try_counter} -lt ${counterRetryPowerOff} ]] && {
      __try_counter=$((__try_counter+1))
      return 0
    }
    # reset
    __try_counter=1
    return 1
  fi
}