#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-dc-hetzner.
#
# rmt-dc-hetzner is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-dc-hetzner is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-dc-hetzner. If not, see <https://www.gnu.org/licenses/>.

############################
# INCLUDES
############################

# currently, dotfiles to test reside one level above bats files
declare -g -r __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../rmt-dc-hetzner-print.dotfile"

############################
# TESTS
############################

@test "invoke print-line" {
  run print-line 'word'
  [ ${status} -eq 0 ]
  [ "${output}" == 'word' ]
  
  run print-line 'a sentence'
  [ ${status} -eq 0 ]
  [ "${output}" == 'a sentence' ]
}

@test "invoke print-line - no args" {
  run print-line
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke print-list-entries" {
  local wordList=()
  run print-list-entries wordList
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  wordList=("word")
  run print-list-entries wordList
  [ ${status} -eq 0 ]
  [ "${output}" == 'word' ]

  wordList=("word1" "word2" "word3")
  run print-list-entries wordList
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'word1' ]
  [ "${lines[1]}" == 'word2' ]
  [ "${lines[2]}" == 'word3' ]
  
  wordList=("more words" "word2" "word3")
  run print-list-entries wordList
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'more words' ]
  [ "${lines[1]}" == 'word2' ]
  [ "${lines[2]}" == 'word3' ]
}


@test "invoke print-list-entries - no args" {
  run print-list-entries
  [ ${status} -eq 1 ]
  [ -z "${output}" ]
}

@test "invoke print-server-info" {
  local wordList=()
  run print-server-info 'testhost' '127.0.0.1' 'cx11' 'nrbg' '0' 'key1, key2, key3'
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'hetzner server:' ]
  [ "${lines[2]}" == 'name        : testhost' ]
  [ "${lines[3]}" == 'ip          : 127.0.0.1' ]
  [ "${lines[4]}" == 'type        : cx11' ]
  [ "${lines[5]}" == 'location    : nrbg' ]
  [ "${lines[6]}" == 'rescue mode : 0' ]
  [ "${lines[7]}" == 'ssh-auth    : key1, key2, key3' ]
}

@test "invoke print-server-info - no args" {
  local wordList=()
  run print-server-info
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'hetzner server:' ]
  [ "${lines[2]}" == 'name        : ' ]
  [ "${lines[3]}" == 'ip          : ' ]
  [ "${lines[4]}" == 'type        : ' ]
  [ "${lines[5]}" == 'location    : ' ]
  [ "${lines[6]}" == 'rescue mode : ' ]
  [ "${lines[7]}" == 'ssh-auth    : ' ]
}

@test "invoke print-server-info-rescue" {
  local wordList=()
  run print-server-info-rescue 'testhost' '127.0.0.1' 'cx11' 'nrbg' 'rescuepwd'
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'hetzner server (rescue mode):' ]
  [ "${lines[2]}" == 'name        : testhost' ]
  [ "${lines[3]}" == 'ip          : 127.0.0.1' ]
  [ "${lines[4]}" == 'type        : cx11' ]
  [ "${lines[5]}" == 'location    : nrbg' ]
  [ "${lines[6]}" == 'rescue-pwd  : rescuepwd' ]
}

@test "invoke print-server-info-rescue - no args" {
  local wordList=()
  run print-server-info-rescue
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'hetzner server (rescue mode):' ]
  [ "${lines[2]}" == 'name        : ' ]
  [ "${lines[3]}" == 'ip          : ' ]
  [ "${lines[4]}" == 'type        : ' ]
  [ "${lines[5]}" == 'location    : ' ]
  [ "${lines[6]}" == 'rescue-pwd  : ' ]
}

@test "invoke print-output - silent" {
  run print-output 'silent' 'testhost' '127.0.0.1' 'nrbg' 'cx11' 'key1, key2, key3' '0' '0'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run print-output 'silent' 'testhost' '127.0.0.1' 'nrbg' 'cx11' 'key1, key2, key3' '1' '0'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  run print-output 'silent' 'testhost' '127.0.0.1' 'nrbg' 'cx11' 'key1, key2, key3' '0' '1'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]

  run print-output 'silent' 'testhost' '127.0.0.1' 'nrbg' 'cx11' 'key1, key2, key3' '1' '1'
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
}

@test "invoke print-output - keyvalue" {
  run print-output 'keyvalue' 'testhost' '127.0.0.1' 'nrbg' 'cx11' 'key1, key2, key3' '0' '0'
  [ ${status} -eq 0 ]
  [ "${output}" == 'testhost 127.0.0.1' ]
  
  run print-output 'keyvalue' 'testhost' '127.0.0.1' 'nrbg' 'cx11' 'key1, key2, key3' '1' '0'
  [ ${status} -eq 0 ]
  [ "${output}" == 'testhost 127.0.0.1 ' ]

  run print-output 'keyvalue' 'testhost' '127.0.0.1' 'nrbg' 'cx11' 'key1, key2, key3' '0' '1'
  [ ${status} -eq 0 ]
  [ "${output}" == 'testhost 127.0.0.1' ]

  run print-output 'keyvalue' 'testhost' '127.0.0.1' 'nrbg' 'cx11' 'key1, key2, key3' '1' '1'
  [ ${status} -eq 0 ]
  [ "${output}" == 'testhost 127.0.0.1' ]
}