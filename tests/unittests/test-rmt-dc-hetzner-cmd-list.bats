#!/usr/bin/env bats

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-dc-hetzner.
#
# rmt-dc-hetzner is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-dc-hetzner is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-dc-hetzner. If not, see <https://www.gnu.org/licenses/>.

############################
# INCLUDES
############################

# currently, dotfiles to test reside one level above bats files
declare -r -g __source_dir="${BATS_TEST_DIRNAME}"

load "${__source_dir}/../../rmt-dc-hetzner-hcloud.dotfile"
load "${__source_dir}/../../rmt-dc-hetzner-cmd-list.dotfile"

load "${__source_dir}/../../../common-test/test-rmt-mocks-common"
load "${__source_dir}/../../../common-test/test-rmt-utilities"

load "${__source_dir}/test-rmt-mocks-hcloud"

############################
# GLOBAL DATA
############################

declare -r -g __test_data_dir=$(mktemp -d -u)

############################
# SETUP
############################

setup() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
  mkdir -p "${__test_data_dir}"

  # overwrite definition in test-rmt-mocks-common
__test_file_buffer="${__test_data_dir}/test-hcloud-file_buffer"
}

teardown() {
  [[ -d "${__test_data_dir}" ]] && rm -r "${__test_data_dir}"
}

############################
# TESTS
############################

@test "invoke handle-list-server - options only" {
  __hcloud_cmd=mock-cmd-ok-append-args-to-file

  local listOut=()
  run handle-list-server
  [ ${status} -eq 0 ]
  
  local optsList=()
  read-file-buffer "${__test_file_buffer}" optsList
  
  [ "${optsList[0]}" == 'server' ]
  [ "${optsList[1]}" == 'list' ]
  [ "${optsList[2]}" == '-o' ]
  [ "${optsList[3]}" == 'noheader' ]
  
  rm "${__test_file_buffer}"
  
  run handle-list-server -v
  [ ${status} -eq 0 ]
  
  optsList=()
  read-file-buffer "${__test_file_buffer}" optsList

  [ "${optsList[0]}" == 'context' ]
  [ "${optsList[1]}" == 'list' ]
  [ "${optsList[2]}" == '-o' ]
  [ "${optsList[3]}" == 'noheader' ]
  [ "${optsList[4]}" == 'server' ]
  [ "${optsList[5]}" == 'list' ]
  [ "${optsList[6]}" == '-o' ]
  [ "${optsList[7]}" == 'noheader' ]
}

@test "invoke handle-list-server" {
  __hcloud_cmd=mock-hcloud-list-server-one

  run handle-list-server 'testhost'
  [ ${status} -eq 0 ]
  [ "${output}" == '34gf76sde54 testhost 127.0.0.1' ]
  
  __hcloud_cmd=mock-hcloud-list-server-three
  
  run handle-list-server 'host'
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '34gf76sde54 localhost 127.0.0.1' ]
  [ "${lines[1]}" == '34gf76sde55 testhost 127.0.0.2' ]
  [ "${lines[2]}" == '34gf76sde56 examplehost 127.0.0.3' ]

  run handle-list-server 'example'
  [ ${status} -eq 0 ]
  [ "${output}" == '34gf76sde56 examplehost 127.0.0.3' ]
  
  run handle-list-server 'local' 'test'
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '34gf76sde54 localhost 127.0.0.1' ]
  [ "${lines[1]}" == '34gf76sde55 testhost 127.0.0.2' ]
}

@test "invoke handle-list-server - no args" {
  __hcloud_cmd=mock-hcloud-list-server-three-and-active-user-context

  run handle-list-server
  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == '34gf76sde54 localhost 127.0.0.1' ]
  [ "${lines[1]}" == '34gf76sde55 testhost 127.0.0.2' ]
  [ "${lines[2]}" == '34gf76sde56 examplehost 127.0.0.3' ]
  
  run handle-list-server -v
  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'active project [testuser]' ]
  [ "${lines[1]}" == '34gf76sde54 localhost 127.0.0.1' ]
  [ "${lines[2]}" == '34gf76sde55 testhost 127.0.0.2' ]
  [ "${lines[3]}" == '34gf76sde56 examplehost 127.0.0.3' ]
}

@test "invoke handle-list-server - no match" {
  __hcloud_cmd=mock-hcloud-list-server-three-and-active-user-context

  run handle-list-server 'unknown'
  # printf '# l1 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run handle-list-server -v 'unknown'
  # printf '# l2 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'active project [testuser]' ]
  [ "${lines[1]}" == 'debug: no running servers' ]
  
  __hcloud_cmd=mock-cmd-ok-no-output

  run handle-list-server 'testhost'
  # printf '# l3 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ -z "${output}" ]
  
  run handle-list-server -v 'testhost'
  # printf '# l4 %s\n' "${lines[@]}" >&3
  [ ${status} -eq 0 ]
  [ "${lines[0]}" == 'active project [could not be detected]' ]
  [ "${lines[1]}" == 'debug: no running servers' ]
}

@test "invoke handle-list-server - error" {
  skip 'error not yet detectable'
  __hcloud_cmd=mock-cmd-error

  run handle-list-server
  [ ${status} -eq 1 ]
}